/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : main.js
 * @Created_at : 14/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import Vue from 'vue'
import App from './App.vue'

// --------------------------------
// Configs

Vue.config.productionTip = false;
Vue.config.performance = true;

// --------------------------------
// Entry Point

let rootElement = '#app';
new Vue({
  render: h => h(App)
}).$mount(rootElement);
