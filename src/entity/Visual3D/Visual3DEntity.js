/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : Visual3DEntity.js
 * @Created_at : 14/10/2019
 * @Update_at : 19/10/2019
 * --------------------------------------------------------------------------
 */

import {Color, Object3D, Vector2, Vector3, WebGLRenderer} from "three";

import MouseEvent from "../../services/Event/MouseEvent";
import ObjectManager from "../../services/Manager/ObjectManager";
import PropertyManager from "../../services/Manager/PropertyManager";
import TypeManager from "../../services/Manager/TypeManager";
import Logger from "../../services/Log/Logger";

import Visual3DBuilder from "../../services/Builder/Visual3D/Visual3DBuilder";
import CrosshairBuilder from "../../services/Builder/ThreeJs/Custom/Crosshair/CrosshairBuilder";
import ObjectBuilder from "../../services/Builder/ThreeJs/Object/ObjectBuilder";
import LineBuilder from "../../services/Builder/ThreeJs/Object/Line/LineBuilder";
import KeyboardEvent from "../../services/Event/KeyboardEvent";
import ModelBuilder from "../../services/Builder/ThreeJs/Object/Model/ModelBuilder";
import Animation from "../../services/Animation/Animation";

export default class Visual3DEntity {
    /**
     * Visual 3D Entity Constructor.
     *
     * @param threeJsContainer
     * @param options
     */
    constructor(threeJsContainer, options = {}) {
        if (!TypeManager.isElement(threeJsContainer)) {
            throw new Error("Variable 'threeJsContainer' need to be an HTML DOM Element.");
        }
        
        this.threeJsContainer = threeJsContainer;
        this.options = Visual3DBuilder.buildOptions(options);
        
        // Init
        this.init();
    }
    
    // ------------------------
    // Init
    
    /**
     * Init Entity.
     */
    init() {
        // ----------------
        // Attributes
        
        this.requestAnimationFrameId = null;
        this.mouseEvent = new MouseEvent(100);
        this.nbObjects = 0;
        this.nbObjectsLoaded = 0;
        this.hasAnimation = true;
        this.hasShortcut = false;
        this.hasMultiSelection = false;
        
        // ----------------
        // Objects
        
        this.lines = {};
        this.objects = {};
        this.selectedObjects = {};
        this.animatedObjectIds = [];
        
        // ----------------
        // Init ThreeJs System.
        
        this.camera = Visual3DBuilder.buildCamera(this.getContainerAspect(), this.options.camera);
        this.scene = Visual3DBuilder.buildScene(this.options.scene);
        this.raycaster = Visual3DBuilder.buildRaycaster();
        this.renderer = Visual3DBuilder.buildRenderer(this.getContainerWidth(), this.getContainerHeight(), this.options.renderer);
        
        // ----------------
        // Add the renderer to the DOM.
        
        this.renderer.domElement.tabIndex = 0;
        this.threeJsContainer.appendChild(this.renderer.domElement);
        
        // ----------------
        // Init ThreeJs 3D Environment.
        
        this.mouse = new Vector2();
        
        this.scene.add(this.camera);
        
        this.control = Visual3DBuilder.buildControls(this.camera, this.renderer, this.options.camera);
        this.control.enabled = this.getEnableOption(this.options.control, true);
        
        this.options.lights.forEach((lOptions) => {
            Visual3DBuilder.buildLight(lOptions)
                .then((light) => {
                    if (TypeManager.isNull(light)) return;
                    let attached = ObjectManager.hasProperty(lOptions, 'attached') ? lOptions.attached : null;
                    
                    switch (attached) {
                        case 'camera':
                            this.camera.add(light);
                            break;
                        
                        default:
                        case 'scene':
                            this.scene.add(light);
                            break;
                    }
                })
                .catch(Logger.error)
            ;
        });
        
        this.crosshair = null;
        let crosshairEnable = this.getEnableOption(this.options.crosshair, false);
        if (crosshairEnable) {
            CrosshairBuilder.build(this.options.crosshair)
                .then((crosshair) => {
                    this.crosshair = crosshair;
                    this.camera.add(this.crosshair);
                })
                .catch(Logger.error)
            ;
        }
        
        if (ObjectManager.hasProperty(this.options, 'fog')) {
            Visual3DBuilder.buildFog(this.options.fog).then(fog => this.scene.fog = fog);
        }
        
        // ----------------
        // Get Callbacks
        
        this.callback_controlChange = this.options.callbacks.controlChange;
        this.callback_clickObject = this.options.callbacks.clickObject;
        this.callback_dblClickObject = this.options.callbacks.dblClickObject;
        
        // ----------------
        // Event Listener
        
        this.control.addEventListener('change', this.event_controlChange.bind(this));
        
        this.renderer.domElement.addEventListener('mousemove', this.event_mouseMove.bind(this), false);
        this.renderer.domElement.addEventListener('click', this.event_click.bind(this), false);
        this.renderer.domElement.addEventListener('dblclick', this.event_dblClick.bind(this), false);
        this.renderer.domElement.addEventListener('keyup', this.event_keyUp.bind(this), false);
        
        window.addEventListener('resize', this.event_windowResize.bind(this), false);
    }
    
    /**
     * Destroy Entity.
     */
    dispose() {
        // ----------------
        // Remove : Render Loop System.
        
        cancelAnimationFrame(this.requestAnimationFrameId);
        
        // ----------------
        // Remove : Event Listener
        
        this.control.removeEventListener('change', this.event_controlChange.bind(this));
        
        this.renderer.domElement.removeEventListener('mousemove', this.event_mouseMove.bind(this), false);
        this.renderer.domElement.removeEventListener('click', this.event_click.bind(this), false);
        this.renderer.domElement.removeEventListener('dblclick', this.event_dblClick.bind(this), false);
        this.renderer.domElement.removeEventListener('keyup', this.event_keyUp.bind(this), false);
        
        window.removeEventListener('resize', this.event_windowResize.bind(this), false);
        
        // ----------------
        // Clear : Callbacks
        
        this.callback_controlChange = null;
        this.callback_clickObject = null;
        this.callback_dblClickObject = null;
        
        // ----------------
        // Clear : ThreeJs System.
        
        this.control.dispose();
        this.scene.dispose();
        if (ObjectManager.is(this.renderer, WebGLRenderer)) this.renderer.dispose();
        
        this.camera = null;
        this.scene = null;
        this.raycaster = null;
        this.renderer = null;
        this.control = null;
        this.crosshair = null;
        
        // ----------------
        // Clear : Objects
        
        this.lines = null;
        this.objects = null;
        this.selectedObjects = null;
        this.animatedObjectIds = null;
        
        // ----------------
        // Clear : Attributes
        
        this.requestAnimationFrameId = null;
        this.mouseEvent = null;
        this.nbObjects = null;
        this.nbObjectsLoaded = null;
        this.hasAnimation = null;
        this.hasShortcut = null;
        this.hasMultiSelection = null;
    }
    
    // ------------------------
    // Boot
    
    /**
     * Launch PU.
     */
    boot() {
        this.render();
    }
    
    // ------------------------
    // Render Methods
    
    /**
     * Render of Visual 3D.
     */
    render() {
        this.requestAnimationFrameId = requestAnimationFrame(this.render.bind(this));
        
        // Resize Renderer
        this.resizeRenderer();
        
        // Animation
        this.animateObjects();
        
        // Lines
        this.syncLines();
        
        // Update Control
        this.control.update();
        
        // Render View
        this.renderer.render(this.scene, this.camera);
    }
    
    /**
     * Animate Objects.
     */
    async animateObjects() {
        if (!this.hasAnimation) return;
        if (this.nbObjectsLoaded !== this.nbObjects) return;
        
        for (let index in this.animatedObjectIds) {
            let id = this.animatedObjectIds[index];
            
            if (!ObjectManager.hasProperty(this.objects, id)) continue;
            
            let object = this.objects[id];
            let name = ObjectManager.hasProperty(object, 'animate') ? object.animate : null;
            Animation.animate(name, object);
        }
    }
    
    /**
     * Synchronize link between objects.
     */
    async syncLines() {
        for (let index in this.lines) {
            let line = this.lines[index];
            
            if (!TypeManager.isArray(line.geometry.vertices) || line.geometry.vertices.length !== 2) continue;
            if (!ObjectManager.hasProperty(line, 'to') || !ObjectManager.hasProperty(line.to, 'id')) continue;
            if (!ObjectManager.hasProperty(line, 'from') || !ObjectManager.hasProperty(line.from, 'id')) continue;
            
            let toId = line.to.id;
            let fromId = line.from.id;
            
            let toObject = ObjectManager.hasProperty(this.objects, toId) ? this.objects[toId] : null;
            let fromObject = ObjectManager.hasProperty(this.objects, fromId) ? this.objects[fromId] : null;
            
            if (TypeManager.isNull(toObject) || TypeManager.isNull(fromObject)) continue;
            
            let verticeTo = line.geometry.vertices[0];
            let verticeFrom = line.geometry.vertices[1];
            
            verticeTo.x = toObject.position.x;
            verticeTo.y = toObject.position.y;
            verticeTo.z = toObject.position.z;
            
            verticeFrom.x = fromObject.position.x;
            verticeFrom.y = fromObject.position.y;
            verticeFrom.z = fromObject.position.z;
            
            line.geometry.verticesNeedUpdate = true;
        }
    }
    
    // ------------------------
    // Getters
    
    /**
     * Get the ThreeJs Container width.
     *
     * @return number
     */
    getContainerWidth() {
        return this.threeJsContainer.offsetWidth;
    }
    
    /**
     * Get the ThreeJs Container height.
     *
     * @return number
     */
    getContainerHeight() {
        return this.threeJsContainer.offsetHeight;
    }
    
    /**
     * Get the ThreeJs Container aspect.
     *
     * @return number
     */
    getContainerAspect() {
        return this.getContainerWidth() / this.getContainerHeight();
    }
    
    /**
     * Get the enable option.
     *
     * @param options
     * @param defaultValue
     * @returns boolean
     */
    getEnableOption(options = {}, defaultValue = false) {
        if (!ObjectManager.hasProperty(options, 'enable')) return defaultValue;
        return PropertyManager.getBoolean(options.enable, defaultValue);
    }
    
    /**
     * Get Crosshair position.
     *
     * @return Vector3
     */
    getCrosshairPosition() {
        let position = new Vector3();
        if (!TypeManager.isNull(this.crosshair)) this.crosshair.getWorldPosition(position);
        return position;
    }
    
    /**
     * @return number
     */
    getNbObjects() {
        return this.nbObjects;
    }
    
    /**
     * Get the intersection with objects in the 3D view on the cursor position.
     * Use Raycaster.
     *
     * @return {*}|null
     */
    getIntersection() {
        let intersections = this.raycaster.intersectObjects(ObjectManager.transformToArrayValues(this.objects), true);
        return (intersections.length) > 0 ? intersections[0] : null;
    }
    
    /**
     * Get the intersection object.
     * Use Raycaster.
     *
     * @return {*}|null
     */
    getIntersectionObject() {
        let intersection = this.getIntersection();
        
        if (!this.checkIntersection(intersection)) return null;
        if (!ObjectManager.hasProperty(intersection, 'object')) return null;
        
        let object = intersection.object;
        
        // Check for the object's format
        if (ObjectManager.hasProperty(object, 'format') && ObjectManager.hasProperty(object, 'parent')) {
            if (object.format === ModelBuilder.EXTENSION_FBX) object = object.parent;
        }
        
        return object;
    }
    
    /**
     * Check if the object is selected.
     *
     * @param id
     * @returns boolean
     */
    isObjectSelected(id) {
        return ObjectManager.hasProperty(this.selectedObjects, id);
    }
    
    /**
     * Get the first selected object.
     *
     * @return {*}|null
     */
    getFirstSelectedObject() {
        if (TypeManager.isEmpty(this.selectedObjects)) return null;
        
        let keys = ObjectManager.transformToArrayKeys(this.selectedObjects);
        if (keys.length < 1) return null;
        
        let key = keys[0];
        if (!ObjectManager.hasProperty(this.selectedObjects, key)) return null;
        
        return this.selectedObjects[key];
    }
    
    // ------------------------
    // Actions
    
    /**
     * Focus on the Scene.
     *
     * @return Visual3DEntity
     */
    focus() {
        if (!ObjectManager.hasMethod(this.renderer.domElement, 'focus')) return this;
        this.renderer.domElement.focus();
        return this;
    }
    
    /**
     * Enable Animation.
     *
     * @return Visual3DEntity
     */
    enableAnimation() {
        this.hasAnimation = true;
        return this;
    }
    
    /**
     * Disable Animation.
     *
     * @return Visual3DEntity
     */
    disableAnimation() {
        this.hasAnimation = false;
        return this;
    }
    
    /**
     * Enable Control.
     *
     * @return Visual3DEntity
     */
    enableControl() {
        if (TypeManager.isNull(this.control)) return this;
        this.control.enabled = true;
        
        return this;
    }
    
    /**
     * Disable Control.
     *
     * @return Visual3DEntity
     */
    disableControl() {
        if (TypeManager.isNull(this.control)) return this;
        this.control.enabled = false;
        
        return this;
    }
    
    /**
     * Enable Shortcut.
     *
     * @return Visual3DEntity
     */
    enableShortcut() {
        this.hasShortcut = true;
        return this;
    }
    
    /**
     * Disable Shortcut.
     *
     * @return Visual3DEntity
     */
    disableShortcut() {
        this.hasShortcut = false;
        return this;
    }
    
    /**
     * Enable Multi Selection.
     *
     * @return Visual3DEntity
     */
    enableMultiSelection() {
        this.hasMultiSelection = true;
        return this;
    }
    
    /**
     * Disable Multi Selection.
     *
     * @return Visual3DEntity
     */
    disableMultiSelection() {
        this.hasMultiSelection = false;
        return this;
    }
    
    /**
     * Check if the intersection contains objects.
     *
     * @param intersection
     * @return boolean
     */
    checkIntersection(intersection) {
        return !TypeManager.isNull(intersection)
            && ObjectManager.hasProperty(intersection, 'object');
    }
    
    /**
     * Check if the object is an interactive object.
     *
     * @param object
     * @return boolean
     */
    checkObjectIntersect(object) {
        return !TypeManager.isNull(object)
            && ObjectManager.hasProperty(object, 'objectId')
            && ObjectManager.hasProperty(object, 'clickable')
            && object.clickable;
    }
    
    resizeRenderer() {
        if (TypeManager.isNull(this.camera)) return;
        if (TypeManager.isNull(this.renderer)) return;
        
        this.renderer.setSize(this.getContainerWidth(), this.getContainerHeight());
        
        this.camera.aspect = this.getContainerAspect();
        this.camera.updateProjectionMatrix();
    }
    
    /**
     * Reset Camera.
     * - Postion
     * - Rotation
     * - Control
     */
    resetCamera() {
        if (TypeManager.isNull(this.camera)) return;
        if (TypeManager.isNull(this.control)) return;
        
        this.camera.position.set(0, 0, 300);
        this.camera.rotation.set(0, 0, 0);
        this.camera.up.set(0, 1, 0);
        this.control.target.set(0, 0, 0);
    }
    
    /**
     * Reset POV.
     * TODO : Rework this action.
     */
    resetPOV() {
        if (TypeManager.isNull(this.camera)) return;
        if (TypeManager.isNull(this.control)) return;
        
        let crosshairVector = this.getCrosshairPosition();
    
        let x = Math.trunc(Math.abs(crosshairVector.x));
        let y = Math.trunc(Math.abs(crosshairVector.y));
    
        this.camera.position.set(x, y, 300);
        this.camera.rotation.set(0, 0, 0);
        this.camera.up.set(0, 1, 0);
        this.control.target.set(x, y, 0);
    }
    
    /**
     * Focus on Object.
     *
     * @param object
     */
    focusOnObject(object) {
        if (TypeManager.isUndefined(object)
            || TypeManager.isNull(object)
            || !ObjectManager.hasProperty(object, 'position')
            || !ObjectManager.hasProperty(object.position, 'x')
            || !ObjectManager.hasProperty(object.position, 'y')
            || !ObjectManager.hasProperty(object.position, 'z')
        ) return;
        if (TypeManager.isNull(this.camera)) return;
        if (TypeManager.isNull(this.control)) return;
        
        let x = object.position.x;
        let y = object.position.y;
        let z = object.position.z;
        let zCamera = z + 25;
        
        this.camera.position.set(x, y , zCamera);
        this.camera.rotation.set(0, 0, 0);
        this.control.target.set(x, y, z);
    }
    
    // ------------------------
    // Object
    
    /**
     * Build Objects.
     *
     * @param objects
     * @param config
     */
    buildObjects(objects = [], config = {}) {
        if (!TypeManager.isArray(objects)) return;
        config = TypeManager.isObject(config) ? config : {};
        
        objects.forEach((options) => {
            this.buildObject(options, config);
        });
    }
    
    /**
     * Build Lines.
     *
     * @param lines
     */
    buildLines(lines = []) {
        if (!TypeManager.isArray(lines)) return;
        
        lines.forEach((options) => {
            this.buildLine(options);
        });
    }
    
    /**
     * Build Object.
     *
     * @param options
     * @param config
     */
    // eslint-disable-next-line
    buildObject(options = {}, config = {}) {
        if (!TypeManager.isObject(options)) return;
        
        this.nbObjects += 1;
        config = TypeManager.isObject(config) ? config : {};
        
        ObjectBuilder.build(options)
            .then((object) => {
                this.nbObjectsLoaded += 1;
                this.addObject(object);
    
                // Sub-Objects
                let subObjects = ObjectManager.hasProperty(options, 'subObjects') ? options.subObjects : null;
                this.buildSubObjects(object, subObjects);
                
                // Lights
                // TODO : Call Builder for Lights (ObjectBuilder)
                /*
                let size = this.getSize(options);
                let status = this.getStatus(config, object.status);
                let mainLight = await this.prepareMainLight(size);
                let statusLight = await this.prepareStatusLight(size, status.color, status.intensity);
                mainLight.target = objContainer;
                statusLight.target = objContainer;
                objContainer.add(mainLight);
                objContainer.add(statusLight);
                 */
            })
            .catch((error) => {
                this.nbObjects -= 1;
                Logger.error(error);
            })
        ;
    }
    
    /**
     * Build Sub-Object.
     *
     * @param object
     * @param subObjects
     */
    buildSubObjects(object, subObjects = []) {
        if (!TypeManager.isObject(object) || !ObjectManager.is(object, Object3D)) return;
        if (!TypeManager.isArray(subObjects)) return;
    
        subObjects.forEach((options) => {
            ObjectBuilder.buildSubObject(options)
                .then(subObject => object.add(subObject))
                .catch(Logger.error)
        });
    }
    
    /**
     * Build Line.
     *
     * @param options
     */
    buildLine(options = {}) {
        if (!TypeManager.isObject(options)) return;
        
        LineBuilder.build(options)
            .then(line => this.addLine(line))
            .catch(Logger.error)
        ;
    }
    
    /**
     * Add Object to the scene.
     *
     * @param object
     */
    addObject(object) {
        if (TypeManager.isNull(object)) return;
        if (TypeManager.isNull(this.scene)) return;
        
        let id = ObjectManager.hasProperty(object, 'objectId') ? object.objectId : object.id;
        
        // Display Object
        this.scene.add(object);
        
        // Store Object
        this.objects[id] = object;
        
        // Check if is an animated object
        if (ObjectManager.hasProperty(object, 'animate')) this.addAnimateId(id);
    }
    
    /**
     * Add link between 2 objects.
     *
     * @param line
     */
    addLine(line) {
        if (TypeManager.isNull(line)) return;
        if (TypeManager.isNull(this.scene)) return;
        
        let id = ObjectManager.hasProperty(line, 'objectId') ? line.objectId : line.id;
        
        // Display Line
        this.scene.add(line);
        
        // Store Line
        this.lines[id] = line;
    }
    /**
     * Remove an object of the scene (By Id).
     *
     * @param id
     */
    removeObjectById(id) {
        if (!ObjectManager.hasProperty(this.objects, id)) return;
        this.removeObject(this.objects[id]);
    }
    
    /**
     * Remove an object of the scene (Can remove children).
     *
     * @param object
     * @param isChild
     */
    removeObject(object, isChild = false) {
        if (TypeManager.isNull(object)) return;
        if (!TypeManager.isObject(object)) return;
        
        // Clear all children
        if (ObjectManager.hasProperty(object, 'children') && TypeManager.isArray(object.children)) {
            for (let index in object.children) {
                this.removeObject(object.children[index], true);
            }
        }
        
        // Clean memory
        if (ObjectManager.hasProperty(object, 'geometry')) object.geometry.dispose();
        if (ObjectManager.hasProperty(object, 'material')) object.material.dispose();
        
        // Check if is child
        if (isChild) return;
        
        // Remove object
        let id = ObjectManager.hasProperty(object, 'objectId') ? object.objectId : object.id;
        
        this.scene.remove(object);
        this.removeAnimateId(id);
        PropertyManager.removeInArrayById(this.selectedObjects, id);
        PropertyManager.removeInArrayById(this.objects, id);
        
        this.nbObjects -= 1;
        this.nbObjectsLoaded -= 1;
    }
    
    /**
     * Remove link between 2 objects (By Id).
     *
     * @param id
     */
    removeLineById(id) {
        if (!ObjectManager.hasProperty(this.lines, id)) return;
        this.removeLine(this.lines[id]);
    }
    
    /**
     * Remove link between 2 objects.
     *
     * @param line
     */
    removeLine(line) {
        if (TypeManager.isNull(line)) return;
        if (!TypeManager.isObject(line)) return;
        
        // Clean memory
        if (ObjectManager.hasProperty(line, 'geometry')) {
            line.geometry.dispose();
        }
        if (ObjectManager.hasProperty(line, 'material')) {
            line.material.dispose();
        }
        
        // Remove line
        let id = ObjectManager.hasProperty(line, 'objectId') ? line.objectId : line.id;
        this.scene.remove(line);
        PropertyManager.removeInArrayById(this.lines, id);
    }
    
    /**
     * Add on the animate array, the id of the object to no longer animate.
     *
     * @param id
     */
    addAnimateId(id) {
        if (this.animatedObjectIds.indexOf(id) !== -1) return;
        this.animatedObjectIds.push(id);
    }
    
    /**
     * Remove on the animate array, the id of the object to animate.
     *
     * @param id
     */
    removeAnimateId(id) {
        if (this.animatedObjectIds.indexOf(id) === -1) return;
        PropertyManager.removeInArray(this.animatedObjectIds, id);
    }
    
    /**
     * Select an object. (By Id)
     *
     * @param id
     */
    selectObject(id) {
        if (!ObjectManager.hasProperty(this.objects, id)) return;
        
        let object = this.objects[id];
        if (TypeManager.isNull(object)) return;
    
        // Check Multi Selection
        if (!this.hasMultiSelection) this.deselectAllObjects();
        
        // Get Selected Param
        let color = this.options.params.selectedObject.color;
        let animate = this.options.params.selectedObject.animate;
    
        // Set Selected Param
        if (!TypeManager.isNull(animate)) object.animate = animate;
        if (object.isGroup) {
            object.traverse((child) => {
                if (!child.isMesh) return;
                
                if (!TypeManager.isNull(color) && TypeManager.isHexColor(color)) {
                    child.material.color = new Color(color);
                    child.material.needsUpdate = true;
                }
            });
        } else if (object.isMesh) {
            if (!TypeManager.isNull(color) && TypeManager.isHexColor(color)) {
                object.material.color = new Color(color);
                object.material.needsUpdate = true;
            }
        }
        
        // Select Object
        this.selectedObjects[id] = object;
        
        (ObjectManager.hasProperty(object, 'animate')) ? this.addAnimateId(id) : this.removeAnimateId(id);
    }
    
    /**
     * Deselect an object. (By Id)
     *
     * @param id
     */
    deselectObject(id) {
        if (!ObjectManager.hasProperty(this.objects, id)) return;
        
        let object = this.objects[id];
        if (TypeManager.isNull(object)) return;
        
        // Get Default Param
        let defaultParams = ObjectManager.hasProperty(object, 'default') ? object.default : {};
        let color = ObjectManager.hasProperty(defaultParams, 'color') ? defaultParams.color : null;
        
        // Restore Object Param
        object.animate = ObjectManager.hasProperty(defaultParams, 'animation') ? defaultParams.animation : null;
        if (object.isGroup) {
            object.traverse((child) => {
                if (!child.isMesh) return;
    
                child.material.color = new Color(color);
                child.material.needsUpdate = true;
            });
        } else if (object.isMesh) {
            object.material.color = new Color(color);
            object.material.needsUpdate = true;
        }
        
        // Deselect Object
        PropertyManager.removeInArrayById(this.selectedObjects, id);
        (ObjectManager.hasProperty(object, 'animate')) ? this.addAnimateId(id) : this.removeAnimateId(id);
    }
    
    /**
     * Deselect all objects.
     */
    deselectAllObjects() {
        for (let id in this.selectedObjects) {
            this.deselectObject(id);
        }
    }
    
    /**
     * Click on a object.
     *
     * @param object
     */
    clickObject(object) {
        if (!this.checkObjectIntersect(object)) return;
        
        let objectId = object.objectId;
        let selected = this.isObjectSelected(objectId);
        
        (selected) ? this.deselectObject(objectId) : this.selectObject(objectId);
        
        if (TypeManager.isNull(this.callback_clickObject) || !TypeManager.isFunction(this.callback_clickObject)) return;
        this.callback_clickObject(selected, object);
    }
    
    /**
     * Dbl Click on a object.
     *
     * @param object
     */
    dblClickObject(object) {
        if (!this.checkObjectIntersect(object)) return;
        
        this.deselectAllObjects();
        
        if (TypeManager.isNull(this.callback_dblClickObject) || !TypeManager.isFunction(this.callback_dblClickObject)) return;
        this.callback_dblClickObject(object);
    }
    
    // ------------------------
    // Events
    
    /**
     * Event on : Window Resize.
     */
    event_windowResize() {
        this.resizeRenderer();
    }
    
    /**
     * Event on : Mouse Move.
     *
     * @param event
     */
    event_mouseMove(event) {
        if (TypeManager.isNull(this.raycaster)) return;
        if (TypeManager.isNull(this.renderer)) return;
        
        this.mouse.x = (event.layerX / this.getContainerWidth()) * 2 - 1;
        this.mouse.y = -(event.layerY / this.getContainerHeight()) * 2 + 1;
        
        // Set from camera
        this.raycaster.setFromCamera(this.mouse, this.camera);
        
        // Change cursor for pointer
        this.renderer.domElement.style.cursor = this.checkObjectIntersect(this.getIntersectionObject()) ? 'pointer' : 'auto';
    }
    
    /**
     * Event on : Change (Control)
     * Called when control change.
     */
    event_controlChange() {
        this.resizeRenderer();
        
        if (TypeManager.isNull(this.camera)) return;
        if (TypeManager.isNull(this.callback_controlChange) || !TypeManager.isFunction(this.callback_controlChange)) return;
        
        let crosshairPosition = this.getCrosshairPosition();
        this.callback_controlChange({
            camera: {
                position: {
                    x: Math.trunc(this.camera.position.x),
                    y: Math.trunc(this.camera.position.y),
                    z: Math.trunc(this.camera.position.z)
                }
            },
            crosshair: {
                position: {
                    x: Math.trunc(crosshairPosition.x),
                    y: Math.trunc(crosshairPosition.y),
                    z: Math.trunc(crosshairPosition.z)
                }
            }
        })
    }
    
    /**
     * Event on : Key Up.
     * Shortcut System.
     *
     * @param event
     */
    event_keyUp(event) {
        if (!this.hasShortcut) return;
        
        if (KeyboardEvent.isKey_Z(event)) this.resetCamera();
        else if (KeyboardEvent.isKey_Space(event)) this.resetPOV();
        else if (KeyboardEvent.isKey_F(event)) this.focusOnObject(this.getFirstSelectedObject());
    }
    
    /**
     * Event on : Click
     *
     * @param event
     */
    event_click(event) {
        this.mouseEvent.click(event)
            .then(() => {
                // Focus on the scene
                this.focus();
                
                // Reset Cursor
                this.renderer.domElement.style.cursor = 'auto';
                
                this.clickObject(this.getIntersectionObject());
            })
        ;
    }
    
    /**
     * Event on : Double Click
     *
     * @param event
     */
    event_dblClick(event) {
        this.mouseEvent.dblClick(event)
            .then(() => {
                // Focus on the scene
                this.focus();
                
                // Reset Cursor
                this.renderer.domElement.style.cursor = 'auto';
                
                this.dblClickObject(this.getIntersectionObject());
            })
        ;
    }
}