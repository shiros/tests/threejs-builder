/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : env.js.dist
 * @Created_at : 12/02/2019
 * @Update_at : 03/10/2019
 * --------------------------------------------------------------------------
 */

export default {
    environment: 'dev',
    
    socket: {
        type: 'https', // http or https
        addr: 'socket.dev.spopit.com', // 192.168.1.2
        port: '40437' // 2000
    },
    
    /*
    socket: {
        type: 'http', // http or https
        addr: 'localhost', // 192.168.1.2
        port: '50437' // 2000
    },
     */

    /*
    socket: {
        type: 'https', // http or https
        addr: 'socket.dev.spopit.com', // 192.168.1.2
        port: '40437' // 2000
    },
     */
    
    /*
    socket: {
        type: 'https', // http or https
        addr: 'socket.spopit.com', // 192.168.1.2
        port: '50437' // 2000
    },
     */

    ressources: {
        type: 'https', // http or https
        addr: 'api.dev.spopit.com' // 192.168.1.2
    }
}