/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : GestureEvent.js
 * @Created_at : 03/04/2019
 * @Update_at : 03/04/2019
 * --------------------------------------------------------------------------
 */

export default class GestureEvent {

    constructor() {
        // TODO : Do Something
    }

    // ------------------------
    // Click

    /**
     * Tap Process.
     *
     * @param event
     * @return Promise
     */
    tap(event = null) {
        // TODO : Do Something
    }

    /**
     * Double Tap Process.
     *
     * @param event
     * @return Promise
     */
    dblTap(event = null) {
        // TODO : Do Something
    }
}