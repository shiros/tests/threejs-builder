/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : KeyboardEvent.js
 * @Created_at : 16/02/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

export default class KeyboardEvent {
    // ------------------------
    // Code

    static keyCode_Z = 90;
    static keyCode_F = 70;

    static keyCode_Enter = 13;
    static keyCode_Return = 8;
    static keyCode_Space = 32;
    static keyCode_Echap = 27;

    // ------------------------
    // Get Event Key Code

    static getKeyCode(event) {
        return (typeof event.which === "number") ? event.which : event.keyCode;
    }

    // ------------------------
    // Key Up

    /**
     * Check if the key event is key 'Z'.
     *
     * @param event
     */
    static isKey_Z(event) {
        return this.getKeyCode(event) === this.keyCode_Z;
    }
    
    /**
     * Check if the key event is key 'F'.
     *
     * @param event
     */
    static isKey_F(event) {
        return this.getKeyCode(event) === this.keyCode_F;
    }

    /**
     * Check if the key event is key 'Enter'.
     *
     * @param event
     */
    static isKey_Enter(event) {
        return this.getKeyCode(event) === this.keyCode_Enter;
    }

    /**
     * Check if the key event is key 'Return'.
     *
     * @param event
     */
    static isKey_Return(event) {
        return this.getKeyCode(event) === this.keyCode_Return;
    }

    /**
     * Check if the key event is key 'Space'.
     *
     * @param event
     */
    static isKey_Space(event) {
        return this.getKeyCode(event) === this.keyCode_Space;
    }

    /**
     * Check if the key event is key 'Echap'.
     *
     * @param event
     */
    static isKey_Echap(event) {
        return this.getKeyCode(event) === this.keyCode_Echap;
    }

}