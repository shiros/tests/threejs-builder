/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : MouseEvent.js
 * @Created_at : 02/04/2019
 * @Update_at : 14/10/2019
 * --------------------------------------------------------------------------
 */

import TypeManager from "../Manager/TypeManager";
import ObjectManager from "../Manager/ObjectManager";

export default class MouseEvent {
    
    /**
     * Mouse Event Contructor.
     *
     * @param delay
     */
    constructor(delay = 200) {
        this.timer = 0;
        this.delay = delay;
        this.prevent = false;
    }

    // ------------------------
    // Click

    /**
     * Click Event
     *
     * @param event
     * @return Promise
     */
    click(event = null) {
        if (!TypeManager.isNull(event) && ObjectManager.hasProperty(event, 'preventDefault')) {
            event.preventDefault()
        }

        return new Promise((resolve) => {
            this.timer = setTimeout(() => {
                if (!this.prevent) {
                    resolve(event);
                }

                this.prevent = false;
            }, this.delay);
        });
    }

    /**
     * Double Click Event
     *
     * @param event
     * @return Promise
     */
    dblClick(event = null) {
        if (!TypeManager.isNull(event) && ObjectManager.hasProperty(event, 'preventDefault')) {
            event.preventDefault()
        }

        // Clear Timer
        clearTimeout(this.timer);

        // Set Prevent
        this.prevent = true;

        return new Promise((resolve) => {
            resolve(event);
            this.prevent = false;
        });
    }
}
