/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : PropertyManager.js
 * @Created_at : 09/05/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

import TypeManager from './TypeManager';
import ObjectManager from "./ObjectManager";

export default class PropertyManager {
    // ------------------------
    // Getter
    
    /**
     * Get boolean result for a property.
     * - True or False (Depends of variable content)
     * - Default False
     *
     * @param property
     * @param defaultValue
     *
     * @return boolean
     */
    static getBoolean(property, defaultValue = false) {
        if (TypeManager.isBoolean(property)) {
            return property;
        }
        
        if (TypeManager.isString(property)) {
            return (property === 'true'
                || property === '1'
            );
        }
        
        if (TypeManager.isNumber(property)) {
            return property === 1;
        }
        
        return defaultValue;
    }
    
    
    // ------------------------
    // Merge
    
    /**
     * Merge two object.
     *
     * @param object1
     * @param object2
     * @returns {{}}
     */
    static mergeObject(object1, object2) {
        if (!TypeManager.isObject(object1) || !TypeManager.isObject(object2)) {
            return {};
        }
        
        return {
            ...object1,
            ...object2
        };
    }
    
    /**
     * Merge objects.
     *
     * @param target
     * @param sources
     *
     * @return {*}
     */
    static mergeObjects(target, ...sources) {
        if (!sources.length) return target;
        
        let source = sources.shift();
        if (!TypeManager.isObject(target) || !TypeManager.isObject(source)) return {};
        
        for (let key in source) {
        }
        
        /*
            if (!sources.length) return target;
          const source = sources.shift();
        
          if (isObject(target) && isObject(source)) {
            for (const key in source) {
              if (isObject(source[key])) {
                if (!target[key]) Object.assign(target, { [key]: {} });
                mergeDeep(target[key], source[key]);
              } else {
                Object.assign(target, { [key]: source[key] });
              }
            }
          }
        
          return mergeDeep(target, ...sources);
         */
    }
    
    /**
     * Merge two array.
     *
     * @param array1
     * @param array2
     *
     * @return array
     */
    static mergeArray(array1, array2) {
        if (!TypeManager.isArray(array1) || !TypeManager.isArray(array2)) {
            return [];
        }
        
        return array1.concat(array2);
    }
    
    
    // ------------------------
    // Remove
    
    /**
     * Remove an element in an object.
     *
     * @param object
     * @param propertyName
     *
     * @return object
     */
    static removeInObject(object, propertyName) {
        if (!TypeManager.isObject(object)) return {};
        if (ObjectManager.hasProperty(object, propertyName)) delete object[propertyName];
        return object;
    }
    
    /**
     * Remove an element in an array.
     *
     * @param array
     * @param id
     *
     * @return array
     */
    static removeInArrayById(array, id) {
        if (!TypeManager.isArray(array)) return [];
        if (ObjectManager.hasProperty(array, id)) delete array[id];
        return array;
    }
    
    /**
     * Remove an element in an array.
     *
     * @param array
     * @param value
     *
     * @return array
     */
    static removeInArray(array, value) {
        if (!TypeManager.isArray(array)) return [];
        return array.filter((item) => item !== value);
    }
    
    
    // ------------------------
    // Check
    
    /**
     * Check if value is in array.
     *
     * @param array
     * @param value
     *
     * @return boolean
     */
    static isInArray(array, value) {
        if (!TypeManager.isArray(array)) return false;
        return array.includes(value);
    }
    
    
    // ------------------------
    // Utils
    
    /**
     * Check if a value match with regex
     *
     * @param value
     * @param regexp
     *
     * @return boolean
     */
    static match(value, regexp) {
        if (TypeManager.isNull(value)) return false;
        
        value = value.toString();
        return value.match(regexp) !== null;
    }
    
    /**
     * Truncate a string.
     *
     * @param value
     * @param size
     *
     * @return string
     */
    static truncate(value, size) {
        if (!TypeManager.isString(value)) value = value.toString();
        return (value.length > size) ? `${value.substring(0, size)}...` : value;
    }
    
    /**
     * Make the first letter of string to uppercase
     *
     * @param string
     *
     * @return string
     */
    static ucFirst(string) {
        if (!TypeManager.isString(string)) return null;
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    /**
     * Convert a date in timestamp.
     * If value if null, return current timestamp.
     *
     * @param value
     *
     * @return number
     */
    static convertToTimestamp(value) {
        if (TypeManager.isNull(value)) return Math.floor(new Date().getTime() / 1000);
        return Math.floor(new Date(value).getTime() / 1000);
    }
}