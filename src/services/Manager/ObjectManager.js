/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
        return object instanceof objectClass;
 *
 * @Author : Alexandre Caillot
 *
 * @File : ObjectManager.js
 * @Created_at : 23/10/2018
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

import TypeManager from './TypeManager';

export default class ObjectManager {
    // ------------------------
    // Check Instance
    
    /**
     * Check if object is an instance of {objectClass}
     *
     * @param object
     * @param objectClass
     *
     * @return {boolean}
     */
    static is(object, objectClass) {
        let is = false;

        try {
            return object instanceof objectClass;
        } catch (e) {
            is = false;
        }

        try {
            return object instanceof objectClass.constructor;
        } catch (e) {
            is = false;
        }

        return is;
    }
    
    
    // ------------------------
    // Has

    /**
     * Check if object have property.
     *
     * @param object
     * @param property
     * @param nullable
     *
     * @return {boolean}
     */
    static hasProperty(object, property, nullable = false) {
        return TypeManager.isObject(object) && object.hasOwnProperty(property) && (nullable ? true : object[property] !== null);
    }
    
    /**
     * Check if object have method.
     *
     * @param object
     * @param method
     *
     * @return {boolean}
     */
    static hasMethod(object, method) {
        return TypeManager.isObject(object) && typeof object[method] === 'function';
    }
    
    
    // ------------------------
    // Clone
    
    /**
     * Clone an Object.
     *
     * @param object
     * @returns {{}|any}
     */
    static clone(object) {
        if (!TypeManager.isObject(object)) return {};
        return Object.assign({}, object);
    }
    
    /**
     * Clone an Array.
     *
     * @param array
     * @returns {Array|*}
     */
    static cloneArray(array) {
        if (!TypeManager.isArray(array)) return [];
        return array.slice(0);
    }
    
    
    // ------------------------
    // Transform

    /**
     * Transform Object to Array (Key / Value Pairs).
     *
     * @param object
     *
     * @return {*}
     */
    static transformToArrayKV(object) {
        if (!TypeManager.isObject(object)) return {};
        return Object.entries(object);
    }

    /**
     * Transform Object to Array (Keys).
     *
     * @param object
     *
     * @return {*}
     */
    static transformToArrayKeys(object) {
        if (!TypeManager.isObject(object)) return [];
        return Object.keys(object);
    }

    /**
     * Transform Object to Array (Values).
     *
     * @param object
     *
     * @return {*}
     */
    static transformToArrayValues(object) {
        if (!TypeManager.isObject(object)) return [];
        return Object.values(object);
    }
}