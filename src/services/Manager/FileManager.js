/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : FileManager.js
 * @Created_at : 18/10/2018
 * @Update_at : 17/10/2019
 * --------------------------------------------------------------------------
 */

import $ from "jquery";

export default class FileManager {
    /**
     * Check if path exist. (Async Method)
     *
     * @param path
     * @returns {Promise<string>}
     */
    static existsAsync(path) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: path,
                type: 'GET',
        
                async: true,
                crossDomain: true,
        
                success: () => {
                    resolve(path);
                },
                error: () => {
                    reject(path);
                }
            });
        })
    }
    
    /**
     * Read a file on a path. (Async Method)
     *
     * @param path
     * @returns {Promise<*>}
     */
    static readAsync(path) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: path,
        
                async: true,
                crossDomain: true,
        
                success: (data) => {
                    resolve(data);
                },
                error: (result, status, error) => {
                    reject(result, status, error);
                }
            });
        });
    }
}