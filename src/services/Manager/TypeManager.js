/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : TypeManager.js
 * @Created_at : 22/10/2018
 * @Update_at : 22/10/2018
 * --------------------------------------------------------------------------
 */

export default class TypeManager {
    // ------------------------
    // Primary
    
    /**
     * Check if the value is undefined.
     *
     * @param value
     * @returns {boolean}
     */
    static isUndefined(value) {
        return typeof value === 'undefined';
    }
    
    /**
     * Check if the value is null.
     *
     * @param value
     * @returns {boolean}
     */
    static isNull(value) {
        return value === null;
    }
    
    /**
     * Check if the value is empty.
     *
     * @param value
     * @returns {boolean}
     */
    static isEmpty(value) {
        if (this.isObject(value)) {
            for (let key in value) {
                if (value.hasOwnProperty(key)) return false;
            }
            return true;
        }
        return (value === '' || value.length <= 0);
    }
    
    /**
     * Check if the value is a string.
     *
     * @param value
     * @returns {boolean}
     */
    static isString(value) {
        return typeof value === 'string' || value instanceof String;
    }
    
    /**
     * Check if the value is a number.
     *
     * @param value
     * @returns {boolean}
     */
    static isNumber(value) {
        return typeof value === 'number' && isFinite(value);
    }
    
    /**
     * Check if the value is a boolean.
     *
     * @param value
     * @returns {boolean}
     */
    static isBoolean(value) {
        return typeof value === 'boolean';
    }
    
    /**
     * Check if the value is a date.
     *
     * @param value
     * @returns {boolean}
     */
    static isDate(value) {
        return value instanceof Date;
    }
    
    /**
     * Check if the value is a hexadecimal color.
     *
     * @param value
     * @returns {boolean}
     */
    static isHexColor(value) {
        let regex = /^#(([a-f\d])([a-f\d])([a-f\d])){1,2}$/i;
        return !this.isNull(value) && this.isString(value) && regex.exec(value) !== null;
    }
    
    // ------------------------
    // Complex
    
    /**
     * Check if the value is an object.
     *
     * @param value
     * @returns {*|boolean}
     */
    static isObject(value) {
        return value && typeof value === 'object' && value.constructor !== Array;
    }
    
    /**
     * Check if the value is an array.
     *
     * @param value
     * @returns {*|boolean}
     */
    static isArray(value) {
        return value && typeof value === 'object' && value.constructor === Array;
    }
    
    /**
     * Check if the value is a function.
     *
     * @param value
     * @returns {boolean}
     */
    static isFunction(value) {
        return typeof value === 'function';
    }
    
    /**
     * Check if the value is a JS Node.
     *
     * @param value
     * @returns {*}
     */
    static isNode(value) {
        return (typeof Node === "object")
            ? value instanceof Node
            : value && typeof value === "object" && typeof value.nodeType === "number" && typeof value.nodeName === "string";
    }
    
    static isElement(value) {
        return (typeof HTMLElement === "object")
            ? value instanceof HTMLElement
            : value && typeof value === "object" && typeof value.nodeType === "number" && typeof value.nodeName === "string";
    }
}