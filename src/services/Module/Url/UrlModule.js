/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : UrlModule.js
 * @Created_at : 30/04/2019
 * @Update_at : 10/09/2019
 * --------------------------------------------------------------------------
 */

import env from '../../../env';
// import store from "../../../store/SpopitStore";

import ObjectManager from "../../Manager/ObjectManager";
import TypeManager from "../../Manager/TypeManager";

export default class UrlModule {

    static get resourceRootPath() {
        if (!ObjectManager.hasProperty(env, 'ressources')
            || !ObjectManager.hasProperty(env.ressources, 'type')
            || !ObjectManager.hasProperty(env.ressources, 'addr')
        ) {
            throw new Error('Have you prepared the environment variables correctly ?');
        }

        return `${env.ressources.type}://${env.ressources.addr}`;
    }

    /**
     * Get the Resource Path
     *
     * @param path
     * @param isPublic
     */
    static resourcePath(path, isPublic = false) {
        if (!TypeManager.isString(path)) {
            throw new Error('The path variable needs to be a string');
        }

        // Get info
        // TODO : Uncomment this line when we use the token system.
        // let token = store.getters.user.token;

        // Prepare Path
        path = path.startsWith('/') ? path.substr(1) : path;
        path = path.startsWith('assets/') ? path.substr(7) : path;

        if (path.startsWith('public/')) {
            isPublic = true;
            path = path.substr(7);
        }

        // Construct Resource Path
        let resourcePath = isPublic
            ? `assets/public/${path}`
            : `assets/${path}`

            // TODO : Use Token when protection api is done
            // : `assets/${token}/${path}`
        ;


        return `${this.resourceRootPath}/${resourcePath}`;
    }
}