/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : Logger.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import env from "../../env";

// TODO : Maybe Log in .log all log. Check how to do this.

export default class Logger {
    static log(text) {
        console.log(text);
    }

    static debug(text) {
        if (env.environment === 'dev') {
            console.debug(text);
        }
    }

    static warn(text) {
        if (env.environment === 'dev') {
            console.warn(text);
        }
    }

    static error(text) {
        if (env.environment === 'dev') {
            console.error(text);
        }
    }
}