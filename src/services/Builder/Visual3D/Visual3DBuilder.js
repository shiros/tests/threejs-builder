/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : Visual3DBuilder.js
 * @Created_at : 14/10/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

import ObjectManager from "../../Manager/ObjectManager";
import PropertyManager from "../../Manager/PropertyManager";

import PerspectiveCameraBuilder from "../ThreeJs/Camera/PerspectiveCameraBuilder";
import OrbitControlsBuilder from "../ThreeJs/Control/OrbitControlsBuilder";
import TrackballControlsBuilder from "../ThreeJs/Control/TrackballControlsBuilder";
import AmbientLightBuilder from "../ThreeJs/Light/AmbientLightBuilder";
import DirectionalLightBuilder from "../ThreeJs/Light/DirectionalLightBuilder";
import PointLightBuilder from "../ThreeJs/Light/PointLightBuilder";
import SpotLightBuilder from "../ThreeJs/Light/SpotLightBuilder";
import SceneBuilder from "../ThreeJs/Scene/SceneBuilder";
import WebGLRendererBuilder from "../ThreeJs/Renderer/WebGLRendererBuilder";
import RaycasterBuilder from "../ThreeJs/Raycaster/RaycasterBuilder";
import FogBuilder from "../ThreeJs/Fog/FogBuilder";
import FogExpBuilder from "../ThreeJs/Fog/FogExpBuilder";

export default class Visual3DBuilder {
    // ------------------------
    // Options
    
    /**
     * Build Options of Visual 3D.
     *
     * @param options
     *
     * @returns {*}
     */
    static buildOptions(options = {}) {
        // Pu Param
        let pu = ObjectManager.hasProperty(options, 'pu') ? options.pu : {};
        let selectedObject = ObjectManager.hasProperty(pu, 'selectedObject') ? pu.selectedObject : {};
        
        // Callbacks
        let callbacks = ObjectManager.hasProperty(options, 'callbacks') ? options.callbacks : {};
        
        // Options
        let puOptions = {
            camera: ObjectManager.hasProperty(options, 'camera') ? options.camera : {},
            control: ObjectManager.hasProperty(options, 'control') ? options.control : {},
            crosshair: ObjectManager.hasProperty(options, 'crosshair') ? options.crosshair : {},
            lights: ObjectManager.hasProperty(options, 'lights') ? options.lights : [],
            raycaster: ObjectManager.hasProperty(options, 'raycaster') ? options.raycaster : {},
            renderer: ObjectManager.hasProperty(options, 'renderer') ? options.renderer : {},
            scene: ObjectManager.hasProperty(options, 'scene') ? options.scene : {},
            
            params: {
                selectedObject: {
                    animate: ObjectManager.hasProperty(selectedObject, 'animate') ? selectedObject.animate : null,
                    color: ObjectManager.hasProperty(selectedObject, 'color') ? selectedObject.color : null
                }
            },
            
            callbacks: {
                controlChange: ObjectManager.hasProperty(callbacks, 'controlChange') ? callbacks.controlChange : null,
                clickObject: ObjectManager.hasProperty(callbacks, 'clickObject') ? callbacks.clickObject : null,
                dblClickObject: ObjectManager.hasProperty(callbacks, 'dblClickObject') ? callbacks.dblClickObject : null,
            },
        };
        
        // Fog Options
        if (ObjectManager.hasProperty(options, 'fog')) {
            puOptions = PropertyManager.mergeObject(puOptions, {fog: options.fog});
        }
        
        return puOptions;
    }
    
    
    // ------------------------
    // ThreeJs Elements
    
    /**
     * Build ThreeJs Camera.
     *
     * @param aspect
     * @param options
     * @returns PerspectiveCamera
     */
    static buildCamera(aspect, options = {}) {
        let type = ObjectManager.hasProperty(options, 'type') ? options.type : null;
        
        switch (type) {
            default:
            case 'perspective':
                return PerspectiveCameraBuilder.build(aspect, options);
        }
    }
    
    /**
     * Build ThreeJs Controls.
     *
     * @param camera
     * @param renderer
     * @param options
     * @returns OrbitControls|TrackballControls
     */
    static buildControls(camera, renderer, options = {}) {
        let type = ObjectManager.hasProperty(options, 'type') ? options.type : null;
        
        switch (type) {
            case 'orbit':
                return OrbitControlsBuilder.build(camera, renderer, options);
    
            default:
            case 'trackball':
                return TrackballControlsBuilder.build(camera, renderer, options);
        }
    }
    
    /**
     * Build ThreeJs Fog.
     *
     * @param options
     * @returns {Promise<Fog>|Promise<FogExp2>}
     */
    static buildFog(options = {}) {
        let type = ObjectManager.hasProperty(options, 'type') ? options.type : null;
        
        switch (type) {
            default:
            case 'fog':
                return FogBuilder.build(options);
            
            case 'fogExp':
                return FogExpBuilder.build(options);
        }
    }
    
    /**
     * Build ThreeJs Light.
     *
     * @param options
     * @returns {Promise<PointLight|AmbientLight|SpotLight|DirectionalLight>}
     */
    static async buildLight(options = {}) {
        let type = ObjectManager.hasProperty(options, 'type') ? options.type : null;
        
        switch (type) {
            case 'ambient':
                return AmbientLightBuilder.build(options);
            
            case 'directional':
                return DirectionalLightBuilder.build(options);
            
            case 'point':
                return PointLightBuilder.build(options);
    
            case 'spot':
                return SpotLightBuilder.build(options);
            
            default:
                return Promise.resolve(null);
        }
    }
    
    /**
     * Build ThreeJs Raycaster.
     *
     * @returns Raycaster
     */
    static buildRaycaster() {
        return RaycasterBuilder.build();
    }
    
    /**
     * Build ThreeJs Renderer.
     *
     * @param width
     * @param height
     * @param options
     *
     * @returns Renderer
     */
    static buildRenderer(width, height, options = {}) {
        return WebGLRendererBuilder.build(width, height, options);
    }
    
    /**
     * Build ThreeJs Scene.
     *
     * @param options
     *
     * @returns Scene
     */
    static buildScene(options = {}) {
        return SceneBuilder.build(options);
    }
}