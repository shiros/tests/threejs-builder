/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : SphereBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {SphereBufferGeometry} from 'three';
import AbstractObjectBuilder from "../AbstractObjectBuilder";

import ObjectManager from "../../../../Manager/ObjectManager";

export default class SphereBuilder extends AbstractObjectBuilder {
    // ------------------------
    // Prepare
    
    /**
     * Prepare Size.
     *
     * @param options
     * @returns {{radius: (*|number), nbSegments: (*|number)}}
     */
    static prepareSize(options = {}) {
        let size = this.getSize(options);
        return {
            radius: ObjectManager.hasProperty(size, 'radius') ? Number(size.radius) : 20,
            nbSegments: ObjectManager.hasProperty(size, 'nbSegments') ? Number(size.nbSegments) : 20
        };
    }
    
    // ------------------------
    // Load
    
    /**
     * Load Geometry.
     *
     * @param options
     * @returns {Promise<SphereBufferGeometry>}
     */
    static async loadGeometry(options = {}) {
        let size = this.prepareSize(options);
        return new SphereBufferGeometry(size.radius, size.nbSegments, size.nbSegments);
    }
}