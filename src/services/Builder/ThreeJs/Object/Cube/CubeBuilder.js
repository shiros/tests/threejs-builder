/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : CubeBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {BoxGeometry} from 'three';
import AbstractObjectBuilder from "../AbstractObjectBuilder";

export default class CubeBuilder extends AbstractObjectBuilder {
    // ------------------------
    // Load
    
    /**
     * Load Geometry.
     *
     * @param options
     * @returns {Promise<BoxGeometry>}
     */
    static async loadGeometry(options = {}) {
        let size = this.prepareSize(options);
        return new BoxGeometry(size.x, size.y, size.z);
    }
}