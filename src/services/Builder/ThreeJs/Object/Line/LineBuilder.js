/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : LineBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

import {Line, Geometry, Vector3} from 'three';
import AbstractObjectBuilder from "../AbstractObjectBuilder";

import ObjectManager from "../../../../Manager/ObjectManager";

import MaterialBuilder from "../../Material/MaterialBuilder";

export default class LineBuilder extends AbstractObjectBuilder {
    // ------------------------
    // Build
    
    /**
     * Build Line.
     *
     * @param options
     * @returns {Promise<Line>}
     */
    static async build(options = {}) {
        this.checkOptions(options);
    
        let geometry = await this.loadGeometry();
        let material = await this.loadMaterial(options);
        let vectors = this.prepareVectors(options);
        
        geometry.vertices.push(vectors.to);
        geometry.vertices.push(vectors.from);
    
        let line = new Line(geometry, material);
    
        line.objectId = options.id;
        line.clickable = this.getClickable(options);
        line.castShadow = true;
        line.receiveShadow = true;
    
        line.to = options.to;
        line.from = options.from;
    
        return line;
    }

    // ------------------------
    // Check

    static checkOptions(options = {}) {
        if (!ObjectManager.hasProperty(options, 'id')

            || !ObjectManager.hasProperty(options, 'to')
            || !ObjectManager.hasProperty(options.to, 'id')

            || !ObjectManager.hasProperty(options, 'from')
            || !ObjectManager.hasProperty(options.from, 'id')
        ) throw new Error('Some options are missing.');
    }

    // ------------------------
    // Prepare

    /**
     * Prepare Vectors.
     *
     * @param options
     * @returns {{from: *, to: *}}
     */
    static prepareVectors(options = {}) {
        let to = ObjectManager.hasProperty(options, 'to') ? options.to : {};
        let from = ObjectManager.hasProperty(options, 'from') ? options.from : {};

        let toPosition = this.preparePosition(to);
        let fromPosition = this.preparePosition(from);

        return {
            to: new Vector3(toPosition.x, toPosition.y, toPosition.z),
            from: new Vector3(fromPosition.x, fromPosition.y, fromPosition.z)
        };
    }
    
    // ------------------------
    // Load
    
    /**
     * Load Geometry.
     *
     * @param options
     * @returns {Promise<Geometry>}
     */
    // eslint-disable-next-line no-unused-vars
    static async loadGeometry(options = {}) {
        return new Geometry();
    }
    
    /**
     * Load Material.
     *
     * @param options
     * @returns {Promise<Material>}
     */
    static async loadMaterial(options = {}) {
        let material = this.getMaterial(options);
    
        if (!ObjectManager.hasProperty(material, 'type')) material.type = 'lineBasic';
        if (material.type !== 'lineBasic') material.type = 'lineBasic';
    
        return MaterialBuilder.build(material);
    }
}