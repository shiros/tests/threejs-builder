/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : CylinderBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {CylinderGeometry} from 'three';
import AbstractObjectBuilder from "../AbstractObjectBuilder";

import ObjectManager from "../../../../Manager/ObjectManager";

export default class CylinderBuilder extends AbstractObjectBuilder {
    // ------------------------
    // Prepare
    
    /**
     * Prepare Size.
     *
     * @param options
     * @returns {{radiusBottom: (*|number), radiusTop: (*|number), height: (*|number)}}
     */
    static prepareSize(options = {}) {
        let size = this.getSize(options);
        return {
            radiusTop: ObjectManager.hasProperty(size, 'radiusTop') ? Number(size.radiusTop) : 20,
            radiusBottom: ObjectManager.hasProperty(size, 'radiusBottom') ? Number(size.radiusBottom) : 20,
            height: ObjectManager.hasProperty(size, 'height') ? Number(size.height) : 20,
        };
    }
    
    // ------------------------
    // Load
    
    /**
     * Load Geometry.
     *
     * @param options
     * @returns {Promise<CylinderGeometry>}
     */
    static async loadGeometry(options = {}) {
        let size = this.prepareSize(options);
        return new CylinderGeometry(size.radiusTop, size.radiusBottom, size.height);
    }
}