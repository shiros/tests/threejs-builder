/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : PlaneBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

import {PlaneBufferGeometry} from 'three';
import AbstractObjectBuilder from "../AbstractObjectBuilder";

import ObjectManager from "../../../../Manager/ObjectManager";

export default class PlaneBuilder extends AbstractObjectBuilder {
    // ------------------------
    // Prepare
    
    /**
     * Prepare Size.
     *
     * @param options
     * @returns {{width: (*|number), height: (*|number)}}
     */
    static prepareSize(options = {}) {
        let size = this.getSize(options);
        return {
            width: ObjectManager.hasProperty(size, 'width') ? Number(size.width) : 20,
            height: ObjectManager.hasProperty(size, 'height') ? Number(size.height) : 20
        };
    }
    
    // ------------------------
    // Load
    
    /**
     * Load Geometry.
     *
     * @param options
     * @returns {Promise<PlaneBufferGeometry>}
     */
    static async loadGeometry(options = {}) {
        let size = this.prepareSize(options);
        return new PlaneBufferGeometry(size.width, size.height);
    }
}