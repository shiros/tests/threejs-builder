/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : TextBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 20/10/2019
 * --------------------------------------------------------------------------
 */

import {ShapeBufferGeometry, FontLoader} from "three";
import AbstractObjectBuilder from "../AbstractObjectBuilder";

import FileManager from "../../../../Manager/FileManager";
import ObjectManager from "../../../../Manager/ObjectManager";
import TypeManager from "../../../../Manager/TypeManager";

import MaterialBuilder from "../../Material/MaterialBuilder";
import Logger from "../../../../Log/Logger";

export default class TextBuilder extends AbstractObjectBuilder {
    // ------------------------
    // Prepare

    /**
     * Prepare Text.
     *
     * @param options
     * @returns {{size: (*|number), content: (*|null), font: (*|string)}}
     */
    static prepareText(options = {}) {
        let size = this.getSize(options);
        return {
            content: ObjectManager.hasProperty(options, 'name') ? options.name : null,
            font: ObjectManager.hasProperty(options, 'font') ? options.font : '/fonts/Quicksand/Quicksand-Regular.json',
            size: TypeManager.isString(size) || TypeManager.isNumber(size) ? Number(size) : 20
        };
    }
    
    // ------------------------
    // Load
    
    /**
     * Load Geometry.
     *
     * @param options
     * @returns {Promise<Geometry>}
     */
    // eslint-disable-next-line no-unused-vars
    static async loadGeometry(options = {}) {
        return new Promise((resolve, reject) => {
            let textOptions = this.prepareText(options);
            
            FileManager.existsAsync(textOptions.font)
                .then((path) => {
                    (new FontLoader()).load(
                        path,
                        (font) => {
                           let shape = font.generateShapes(textOptions.content, textOptions.size, textOptions.size);
                
                            let geometry = new ShapeBufferGeometry(shape);
                            geometry.computeBoundingBox();
                
                            // Correction for position
                            let xMid = (geometry.boundingBox.max.x - geometry.boundingBox.min.x) * -0.5;
                            let yMid = (geometry.boundingBox.max.y - geometry.boundingBox.min.y) * -0.5;
                            geometry.translate(xMid, yMid, 0);
                
                            resolve(geometry);
                        },
                        null,
                        reject
                    )
                })
                .catch(Logger.error)
            ;
        });
    }
    
    /**
     * Load Material.
     *
     * @param options
     * @returns {Promise<Material>}
     */
    static async loadMaterial(options = {}) {
        let material = this.getMaterial(options);
        material.type = 'basic';
        material.side = 'double';
    
        return MaterialBuilder.build(material);
    }
}