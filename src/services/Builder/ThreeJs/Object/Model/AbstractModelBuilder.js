/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : AbstractModelBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import AbstractObjectBuilder from "../AbstractObjectBuilder";

import ObjectManager from "../../../../Manager/ObjectManager";

export default class AbstractModelBuilder extends AbstractObjectBuilder {
    
    /**
     * Abstract Model Builder Constructor.
     */
    constructor() {
        super();
        if (this.constructor === AbstractModelBuilder) {
            throw new TypeError('Abstract class "AbstractModelBuilder" cannot be instantiated directly');
        }
    }
    
    // ------------------------
    // Getters
    
    /**
     * Get Model.
     *
     * @param options
     * @returns {string|null}
     */
    static getModel(options = {}) {
        let model = ObjectManager.hasProperty(options, 'model') ? options.model : null;
        if (ObjectManager.hasProperty(model, 'value')) model = model.value;
        
        return model;
    }
}