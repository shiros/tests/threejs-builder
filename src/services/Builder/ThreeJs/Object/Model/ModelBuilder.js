/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : ModelBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

import ObjectManager from "../../../../Manager/ObjectManager";
import TypeManager from "../../../../Manager/TypeManager";

import FBXModelBuilder from "./FBXModelBuilder";
import AbstractObjectBuilder from "../AbstractObjectBuilder";

export default class ModelBuilder {
    // ------------------------
    // Constants
    
    static get EXTENSION_FBX() {
        return 'FBX';
    }
    
    // ------------------------
    // Build
    
    /**
     * Build Model.
     *
     * @param options
     * @returns {Promise<*>}
     */
    static async build(options = {}) {
        let path = this.getModel(options);
        let extension = this.getExtension(path);
        
        switch (extension) {
            case 'fbx':
                return FBXModelBuilder.build(options);

            default:
                throw new Error('Bad Extension');
        }
    }
    
    // ------------------------
    // Getters
    
    /**
     * Get Model.
     *
     * @param options
     * @returns {string|null}
     */
    static getModel(options = {}) {
        let model = ObjectManager.hasProperty(options, 'model') ? options.model : null;
        if (ObjectManager.hasProperty(model, 'value')) model = model.value;
        
        return model;
    }
    
    /**
     * Get Extension of file.
     *
     * @param path
     * @return string|null
     */
    static getExtension(path) {
        if (TypeManager.isNull(path)) return null;
    
        let pathSplit = path.split('.');
        return pathSplit[pathSplit.length - 1];
    }
    
    /**
     * Get Size.
     *
     * @param options
     * @returns {*|number}
     */
    static getSize(options) {
        let path = this.getModel(options);
        let extension = this.getExtension(path);
        
        switch (extension) {
            case 'fbx':
                return FBXModelBuilder.prepareSize(options).x;
        
            default:
                return AbstractObjectBuilder.prepareSize(options).x
        }
    }
}