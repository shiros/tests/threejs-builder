/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : FBXModelBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

import AbstractModelBuilder from "./AbstractModelBuilder";

import {Cache} from 'three';
import {FBXLoader} from 'three/examples/jsm/loaders/FBXLoader';

import ModelBuilder from "./ModelBuilder";

import FileManager from "../../../../Manager/FileManager";
import ObjectManager from "../../../../Manager/ObjectManager";
import TypeManager from "../../../../Manager/TypeManager";
import UrlModule from "../../../../Module/Url/UrlModule";
import Logger from "../../../../Log/Logger";

export default class FBXModelBuilder extends AbstractModelBuilder {
    // ------------------------
    // Build
    
    /**
     * Build Object.
     *
     * @param options
     * @returns {Promise<*>}
     */
    static async build(options = {}) {
        this.checkOptions(options);
        
        let model = this.getModel(options);
        let path = UrlModule.resourcePath(model);
        let material = await this.loadMaterial(options);
        
        let position = this.preparePosition(options);
        let rotation = this.prepareRotation(options);
        let size = this.prepareSize(options);
        
        let animate = this.getAnimate(options);
        let clickable = this.getClickable(options);
        
        let object = await this.loadFBX(path);
        
        object.objectId = options.id;
        object.name = options.name;
        object.status = options.status;
        object.clickable = clickable;
    
        if (!TypeManager.isNull(animate)) object.animate = animate;
    
        object.position.set(position.x, position.y, position.z);
        object.rotation.set(rotation.x, rotation.y, rotation.z);
        object.scale.set(size.x, size.y, size.z);
    
        // Default
        object.default = {
            animate: animate,
            color: this.getColor(options),
            position: {x: position.x, y: position.y, z: position.z},
            rotation: {x: rotation.x, y: rotation.y, z: rotation.z}
        };
    
        object.traverse((child) => {
            if (!child.isMesh) return;
            child.material = material;
            child.castShadow = true;
            child.receiveShadow = true;
            
            // Fix the scale for the parent.
            child.scale.set(1,1,1);

            // Need to raycaster intersect. Because for FBX, the intersect get the child and not the main object.
            child.format = ModelBuilder.EXTENSION_FBX;
        });
        
        return object;
    }
    
    // ------------------------
    // Check
    
    static checkOptions(options = {}) {
        super.checkOptions(options);
        if (!ObjectManager.hasProperty(options, 'model')) {
            throw new Error('Some options are missing');
        }
    }
    // ------------------------
    // Build
    
    /**
     * Load FBX.
     *
     * @param path
     * @returns {Promise<*>}
     */
    static loadFBX(path) {
        return new Promise((resolve, reject) => {
            FileManager.existsAsync(path)
                .then((path) => {
                    // Enable Cache
                    Cache.enabled = true;
            
                    // Get FBX Loader
                    let loader = new FBXLoader();
            
                    // Load
                    loader.load(
                        path,
                        (object) => {
                            resolve(object);
                        },
                        null,
                        (e) => {
                            Logger.error('Error while loading the fbx model.');
                            reject(e);
                        }
                    );
                })
                .catch((path) => {
                    reject(`Error while loading the fbx model. File '${path}' doesn't exist.`);
                })
            ;
        });
    }
}