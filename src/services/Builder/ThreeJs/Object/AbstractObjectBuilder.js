/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : AbstractObjectBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

import {Mesh, BoxGeometry} from 'three';

import TypeManager from "../../../Manager/TypeManager";
import ObjectManager from "../../../Manager/ObjectManager";
import PropertyManager from "../../../Manager/PropertyManager";

import MaterialBuilder from "../Material/MaterialBuilder";

export default class AbstractObjectBuilder {
    
    /**
     * Abstract Object Builder Constructor.
     */
    constructor() {
        if (this.constructor === AbstractObjectBuilder) {
            throw new TypeError('Abstract class "AbstractObjectBuilder" cannot be instantiated directly');
        }
    }
    
    // ------------------------
    // Build
    
    /**
     * Build Object.
     *
     * @param options
     * @returns {Promise<*>}
     */
    static async build(options = {}) {
        this.checkOptions(options);
    
        let geometry = await this.loadGeometry(options);
        let material = await this.loadMaterial(options);
    
        let position = this.preparePosition(options);
        let rotation = this.prepareRotation(options);
    
        let animate = this.getAnimate(options);
    
        let object = new Mesh(geometry, material);
    
        object.objectId = options.id;
        object.name = options.name;
        object.status = options.status;
        object.clickable = this.getClickable(options);
        object.castShadow = true;
        object.receiveShadow = true;
    
        if (!TypeManager.isNull(animate)) object.animate = animate;
        
        object.position.set(position.x, position.y, position.z);
        object.rotation.set(rotation.x, rotation.y, rotation.z);
        
        // Default
        object.default = {
            animate: animate,
            color: this.getColor(options),
            position: {x: position.x, y: position.y, z: position.z},
            rotation: {x: rotation.x, y: rotation.y, z: rotation.z}
        };
    
        return object;
    }
    
    // ------------------------
    // Check
    
    static checkOptions(options = {}) {
        if (!ObjectManager.hasProperty(options, 'id')
            || !ObjectManager.hasProperty(options, 'name')
            || !ObjectManager.hasProperty(options, 'status', true)
        ) throw new Error('Some options are missing.');
    }
    
    // ------------------------
    // Prepare
    
    /**
     * Prepare Position.
     *
     * @param options
     * @returns {{x: (*|number), y: (*|number), z: (*|number)}}
     */
    static preparePosition(options = {}) {
        let position = this.getPosition(options);
        return {
            x: ObjectManager.hasProperty(position, 'x') ? Number(position.x) : 0,
            y: ObjectManager.hasProperty(position, 'y') ? Number(position.y) : 0,
            z: ObjectManager.hasProperty(position, 'z') ? Number(position.z) : 0,
        };
    }
    
    /**
     * Prepare Rotation.
     *
     * @param options
     * @returns {{x: (*|number), y: (*|number), z: (*|number)}}
     */
    static prepareRotation(options = {}) {
        let rotate = this.getRotation(options);
        return {
            x: ObjectManager.hasProperty(rotate, 'x') ? Number(rotate.x) : 0,
            y: ObjectManager.hasProperty(rotate, 'y') ? Number(rotate.y) : 0,
            z: ObjectManager.hasProperty(rotate, 'z') ? Number(rotate.z) : 0,
        };
    }
    
    /**
     * Prepare Size.
     *
     * @param options
     * @returns {{x: (*|number), y: (*|number), z: (*|number)}}
     */
    static prepareSize(options = {}) {
        let size = this.getSize(options);
        return {
            x: ObjectManager.hasProperty(size, 'x') ? Number(size.x) : 20,
            y: ObjectManager.hasProperty(size, 'y') ? Number(size.y) : 20,
            z: ObjectManager.hasProperty(size, 'z') ? Number(size.z) : 20,
        };
    }
    
    // ------------------------
    // Load
    
    /**
     * Load Geometry.
     *
     * @param options
     * @returns {Promise<*>}
     */
    static async loadGeometry(options = {}) {
        throw new Error("You must be override the 'loadGeometry' method before use it.");
    }
    
    /**
     * Load Material.
     *
     * @param options
     * @returns {Promise<Material>}
     */
    static async loadMaterial(options = {}) {
        return MaterialBuilder.build(this.getMaterial(options));
    }
    
    // ------------------------
    // Getters
    
    /**
     * Get Material options.
     *
     * @param options
     * @returns {{}}
     */
    static getMaterial(options = {}) {
        return ObjectManager.hasProperty(options, 'material') ? options.material : {};
    }
    
    /**
     * Get Color options.
     *
     * @param options
     * @param defaultValue
     * @returns {string|null}
     */
    static getColor(options = {}, defaultValue = null) {
        if (!ObjectManager.hasProperty(options, 'material')) return defaultValue;
        
        let color =  ObjectManager.hasProperty(options.material, 'color') ? options.material.color : defaultValue;
        if (ObjectManager.hasProperty(color, 'value')) color = color.value;
        
        return color;
    }
    
    /**
     * Get Animate options.
     *
     * @param options
     * @param defaultValue
     * @returns {string|null}
     */
    static getAnimate(options = {}, defaultValue = null) {
        let animate = ObjectManager.hasProperty(options, 'animate') ? options.animate : defaultValue;
        if (ObjectManager.hasProperty(animate, 'value')) animate = animate.value;
        
        return animate;
    }
    
    /**
     * Get Clickable options.
     *
     * @param options
     * @param defaultValue
     * @returns {boolean}
     */
    static getClickable(options = {}, defaultValue = false) {
        if (!ObjectManager.hasProperty(options, 'clickable')) return defaultValue;
        return PropertyManager.getBoolean(options.clickable, defaultValue);
    }
    
    /**
     * Get Position options.
     *
     * @param options
     * @param defaultValue
     * @returns *
     */
    static getPosition(options = {}, defaultValue = null) {
        let position = (ObjectManager.hasProperty(options, 'position')) ? options.position : defaultValue;
        if (ObjectManager.hasProperty(position, 'value')) position = position.value;
        
        return position;
    }
    
    /**
     * Get Rotation options.
     *
     * @param options
     * @param defaultValue
     * @returns *
     */
    static getRotation(options = {}, defaultValue = null) {
        let rotate = (ObjectManager.hasProperty(options, 'rotate')) ? options.rotate : defaultValue;
        if (ObjectManager.hasProperty(rotate, 'value')) rotate = rotate.value;
        
        return rotate;
    }
    
    /**
     * Get Size options.
     *
     * @param options
     * @param defaultValue
     * @returns *
     */
    static getSize(options = {}, defaultValue = null) {
        let size = (ObjectManager.hasProperty(options, 'size')) ? options.size : defaultValue;
        if (ObjectManager.hasProperty(size, 'value')) size = size.value;
        
        return size;
    }
}