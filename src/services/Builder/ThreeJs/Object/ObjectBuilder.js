/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : ObjectBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 20/10/2019
 * --------------------------------------------------------------------------
 */

import ObjectManager from "../../../Manager/ObjectManager";
import PropertyManager from "../../../Manager/PropertyManager";
import TypeManager from "../../../Manager/TypeManager";

import AbstractObjectBuilder from "./AbstractObjectBuilder";

import CubeBuilder from "./Cube/CubeBuilder";
import CylinderBuilder from "./Cylinder/CylinderBuilder";
import ModelBuilder from "./Model/ModelBuilder";
import PlaneBuilder from "./Plane/PlaneBuilder";
import SphereBuilder from "./Sphere/SphereBuilder";
import TextBuilder from "./Text/TextBuilder"

import SpotLightBuilder from "../Light/SpotLightBuilder";

export default class ObjectBuilder {
    // ------------------------
    // Build
    
    /**
     * Build Object.
     *
     * @param options
     * @returns {Promise<*>}
     */
    static async build(options = {}) {
        let container = this.getContainer(options);
        let geometry = this.getGeometry(options);
        
        let containerOptions = await this.prepareContainerOptions(container, options);
        let objectOptions = await this.prepareObjectOptions(container, options);
        
        // ----------------
        // Object & Container
        
        let objContainer = await this.loadContainer(container, containerOptions);
        let object = await this.loadObject(geometry, objectOptions);
        
        // ----------------
        // Build
        
        (TypeManager.isNull(objContainer))
            ? objContainer = object
            : objContainer.add(object)
        ;
        
        return objContainer;
    }
    
    /**
     * Build Sub-Object.
     *
     * @param options
     * @returns {Promise<*>}
     */
    static async buildSubObject(options = {}) {
        return await this.loadObject(this.getGeometry(options), options);
    }
    
    /**
     * Build Main Object's Light.
     *
     * @param size
     * @returns {Promise<SpotLight>}
     */
    static async buildMainLight(size) {
        return SpotLightBuilder.build({
            color: '#ffffff',
            intensity: 300,
        
            angle: -((size / 2) + 30),
            distance: (size * 2) + 50,
        
            position: {
                x: -((size / 2) + 30),
                y: -((size / 2) + 30),
                z: -((size / 2) + 30)
            }
        });
    }
    
    /**
     * Build Status Object's Light.
     *
     * @param size
     * @param color
     * @param intensity
     * @returns {Promise<SpotLight>}
     */
    static async buildStatusLight(size, color = null, intensity = null) {
        return SpotLightBuilder.build({
            angle: (size / 2) + 30,
            distance: (size * 2) + 50,
    
            position: {
                x: (size / 2) + 30,
                y: (size / 2) + 30,
                z: (size / 2) + 30
            },
    
            color: TypeManager.isNull(color) ? color : '#FFFFFF',
            intensity: TypeManager.isNull(intensity) ? intensity : 150
        });
    }
    
    // ------------------------
    // Getters
    
    /**
     * Get Container.
     *
     * @param options
     * @returns {string|null}
     */
    static getContainer(options = {}) {
        let container = ObjectManager.hasProperty(options, 'container') ? options.container : null;
        if (ObjectManager.hasProperty(container, 'value')) container = container.value;
        
        return container;
    }
    
    /**
     * Get Geometry.
     *
     * @param options
     * @returns {string|null}
     */
    static getGeometry(options = {}) {
        let geometry = ObjectManager.hasProperty(options, 'geometry') ? options.geometry : null;
        if (ObjectManager.hasProperty(geometry, 'value')) geometry = geometry.value;
        
        return geometry;
    }
    
    /**
     * Get Size
     *
     * @param options
     * @returns {*|number|number}
     */
    static getSize(options = {}) {
        let size = null;
        
        switch (this.getGeometry(options)) {
            default:
            case 'cube':
                return CubeBuilder.prepareSize(options).x;
            
            case 'cylinder':
                return CylinderBuilder.prepareSize(options).height;
            
            case 'model':
                return ModelBuilder.getSize(options);
            
            case 'plane':
                size = PlaneBuilder.prepareSize(options);
                if (size.height > size.width) return size.height;
                else return size.width;
            
            case 'sphere':
                return SphereBuilder.prepareSize(options).radius * 2;
        }
    }
    
    /**
     * Get Status.
     *
     * @param config
     * @param objectStatus
     *
     * @return Object
     */
    static getStatus(config = {}, objectStatus) {
        if (TypeManager.isNull(objectStatus)) objectStatus = 0;
        let configStatus = ObjectManager.hasProperty(config, 'status') ? config.status : {};
        let status = ObjectManager.hasProperty(configStatus, objectStatus) ? configStatus[objectStatus] : {};
        
        return {
            color: ObjectManager.hasProperty(status, 'color') ? status.color : null,
            intensity: ObjectManager.hasProperty(status, 'intensity') ? status.intensity : null
        }
    }
    
    // ------------------------
    // Load
    
    /**
     * Load Container.
     *
     * @param container
     * @param options
     * @returns {Promise<*>}
     */
    static loadContainer(container, options = {}) {
        switch (container) {
            case 'sphere':
                return SphereBuilder.build(options);
            
            default:
                return null;
        }
    }
    
    /**
     * Load Object.
     *
     * @param geometry
     * @param options
     * @returns {Promise<*>}
     */
    static async loadObject(geometry, options = {}) {
        switch (geometry) {
            default:
            case 'cube':
                return CubeBuilder.build(options);
            
            case 'cylinder':
                return CylinderBuilder.build(options);
            
            case 'model':
                return ModelBuilder.build(options);
            
            case 'plane':
                return PlaneBuilder.build(options);
            
            case 'text':
                return TextBuilder.build(options);
            
            case 'sphere':
                return SphereBuilder.build(options);
        }
    }
    
    // ------------------------
    // Prepare
    
    /**
     * Prepare Container Options.
     *
     * @param container
     * @param options
     * @returns {Promise<{}>}
     */
    static async prepareContainerOptions(container, options) {
        AbstractObjectBuilder.checkOptions(options);
        
        // Vars
        let size = this.getSize(options);
        let position = AbstractObjectBuilder.preparePosition(options);
        
        // Prepare Options
        let containerOptions = {
            id: options.id,
            name: options.name,
            status: options.status,
            
            animate: AbstractObjectBuilder.getAnimate(options),
            clickable: AbstractObjectBuilder.getClickable(options),
            
            material: {
                side: 'back',
                color: '#FFFFFF',
                roughness: 0.6,
                metalness: 0.8,
                opacity: 0.2,
                transparency: true
            },
            
            position: {x: position.x, y: position.y, z: position.z}
        };
        
        // Setting according to the type of container
        switch (container) {
            case 'sphere':
                return PropertyManager.mergeObject(containerOptions, {
                    size: {radius: size + (size / 4)}
                });
            
            default:
                return PropertyManager.mergeObject(containerOptions, {size: size});
        }
    }
    
    /**
     * Prepare Object Options.
     *
     * @param container
     * @param options
     * @returns {Promise<{}|any>}
     */
    static async prepareObjectOptions(container, options) {
        AbstractObjectBuilder.checkOptions(options);
        
        let objectOptions = ObjectManager.clone(options);
        
        if (!TypeManager.isNull(container)) {
            objectOptions = PropertyManager.mergeObject(objectOptions, {
                position: {x: 0, y: 0, z: 0}
            });
        }
        
        return objectOptions;
    }
}