/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : FogExpBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {FogExp2} from 'three';

import ObjectManager from "../../../Manager/ObjectManager";
import TypeManager from "../../../Manager/TypeManager";

export default class FogExpBuilder {
    /**
     * Build Fog Exp2.
     *
     * @param options
     * @returns {Promise<FogExp2>}
     */
    static async build(options = {}) {
        // Prepare variables
        let color = ObjectManager.hasProperty(options, 'color') && TypeManager.isHexColor(options.color)
            ? options.color : '#FFFFFF';
        let density = ObjectManager.hasProperty(options, 'density') ? Number(options.density) : 0.00025;
        
        // Create Fog Exp
        return new FogExp2(color, density);
    }
}