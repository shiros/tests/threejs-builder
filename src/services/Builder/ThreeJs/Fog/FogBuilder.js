/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : FogBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {Fog} from 'three';

import ObjectManager from "../../../Manager/ObjectManager";
import TypeManager from "../../../Manager/TypeManager";

export default class FogBuilder {
    /**
     * Build Fog.
     *
     * @param options
     * @returns {Promise<Fog>}
     */
    static async build(options = {}) {
        // Prepare variables
        let color = ObjectManager.hasProperty(options, 'color') && TypeManager.isHexColor(options.color)
            ? options.color : '#FFFFFF';
        let near = ObjectManager.hasProperty(options, 'near') ? Number(options.near) : 0;
        let far = ObjectManager.hasProperty(options, 'far') ? Number(options.far) : 1000;
        
        // Create Fog
        return new Fog(color, near, far);
    }
}