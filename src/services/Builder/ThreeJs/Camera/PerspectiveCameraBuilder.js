/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : PerspectiveCameraBuilder.js
 * @Created_at : 14/10/2019
 * @Update_at : 14/10/2019
 * --------------------------------------------------------------------------
 */

import { PerspectiveCamera } from 'three';

import ObjectManager from "../../../Manager/ObjectManager";

export default class PerspectiveCameraBuilder {
    /**
     * Build Perspective Camera.
     *
     * @param aspect
     * @param options
     * @returns {PerspectiveCamera}
     */
    static build(aspect, options = {}) {
        // Prepare variables
        let fov = ObjectManager.hasProperty(options, 'fov') ? Number(options.fov) : 75;
        let near = ObjectManager.hasProperty(options, 'near') ? Number(options.near) : 1;
        let far = ObjectManager.hasProperty(options, 'far') ? Number(options.far) : 10000;

        let position = ObjectManager.hasProperty(options, 'position') ? options.position : {};
        let positionX = ObjectManager.hasProperty(position, 'x') ? Number(position.x) : 0;
        let positionY = ObjectManager.hasProperty(position, 'y') ? Number(position.y) : 0;
        let positionZ = ObjectManager.hasProperty(position, 'z') ? Number(position.z) : 2000;

        // Create Camera
        let camera = new PerspectiveCamera(fov, aspect, near, far);

        // Settings post build
        camera.position.set (positionX, positionY, positionZ);

        return camera;
    }
}