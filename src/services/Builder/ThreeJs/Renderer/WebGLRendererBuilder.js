/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : WebGLRendererBuilder.js
 * @Created_at : 15/10/2018
 * @Update_at : 15/10/2018
 * --------------------------------------------------------------------------
 */
import {WebGLRenderer, PCFSoftShadowMap} from 'three';

import ObjectManager from "../../../Manager/ObjectManager";
import PropertyManager from "../../../Manager/PropertyManager";

export default class WebGLRendererBuilder {
    /**
     * Build WebGl Renderer.
     *
     * @param width
     * @param height
     * @param options
     * @returns {WebGLRenderer}
     */
    static build(width, height, options = {}) {
        // Prepare variables
        let alpha = ObjectManager.hasProperty(options, 'alpha')
            ? PropertyManager.getBoolean(options.alpha) : true;
        let antialias = ObjectManager.hasProperty(options, 'antialias')
            ? PropertyManager.getBoolean(options.antialias) : true;
        let physicallyCorrectLights = ObjectManager.hasProperty(options, 'physicallyCorrectLights')
            ? PropertyManager.getBoolean(options.physicallyCorrectLights) : true;
        
        // Create Renderer
        let renderer = new WebGLRenderer({
            alpha: alpha,
            antialias: antialias
        });
        
        // Settings post build
        renderer.setSize(width, height);
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = PCFSoftShadowMap;
        renderer.powerPreference = 'high-performance';
        renderer.physicallyCorrectLights = physicallyCorrectLights;
        renderer.autoClear = true;
        
        return renderer;
    }
}