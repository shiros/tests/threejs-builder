/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : TrackballControlsBuilder.js
 * @Created_at : 14/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {TrackballControls} from 'three/examples/jsm/controls/TrackballControls';

import ObjectManager from "../../../Manager/ObjectManager";
import PropertyManager from "../../../Manager/PropertyManager";

export default class TrackballControlsBuilder {
    /**
     * Build Trackball Controls.
     *
     * @param camera
     * @param renderer
     * @param options
     * @returns {TrackballControls}
     */
    static build(camera, renderer, options = {}) {
        if (!ObjectManager.hasProperty(renderer, 'domElement')) {
            throw new Error("The renderer haven't the property domElement");
        }
    
        // Prepare variables
        let enableDamping = ObjectManager.hasProperty(options, 'enableDamping')
            ? PropertyManager.getBoolean(options.enableDamping) : false;
        let dampingFactor = ObjectManager.hasProperty(options, 'dampingFactor') ? Number(options.dampingFactor) : 0;
    
        let enableZoom = ObjectManager.hasProperty(options, 'enableZoom')
            ? PropertyManager.getBoolean(options.enableZoom) : true;
        let zoomSpeed = ObjectManager.hasProperty(options, 'zoomSpeed') ? Number(options.zoomSpeed) : 1.2;
    
        let enablePan = ObjectManager.hasProperty(options, 'enablePan')
            ? PropertyManager.getBoolean(options.enablePan) : true;
        let panSpeed = ObjectManager.hasProperty(options, 'panSpeed') ? Number(options.panSpeed) : 1;
    
        let enableRotate = ObjectManager.hasProperty(options, 'enableRotate')
            ? PropertyManager.getBoolean(options.enableRotate) : true;
        let rotateSpeed = ObjectManager.hasProperty(options, 'rotateSpeed') ? Number(options.rotateSpeed) : 4;
    
        let distance = ObjectManager.hasProperty(options, 'distance') ? options.distance : {};
        let minDistance = ObjectManager.hasProperty(distance, 'min') ? Number(distance.min) : 0;
        let maxDistance = ObjectManager.hasProperty(distance, 'max') ? Number(distance.max) : 800;

        // Create Trackball Controls
        let control = new TrackballControls(camera, renderer.domElement);

        // Set up Trackball Controls
        control.staticMoving = !enableDamping;
        control.dynamicDampingFactor = dampingFactor;

        control.noZoom = !enableZoom;
        control.zoomSpeed = zoomSpeed;

        control.noPan = !enablePan;
        control.panSpeed = panSpeed;

        control.noRotate = !enableRotate;
        control.rotateSpeed = rotateSpeed;

        control.minDistance = minDistance;
        control.maxDistance = maxDistance;

        control.update();

        return control;
    }
}