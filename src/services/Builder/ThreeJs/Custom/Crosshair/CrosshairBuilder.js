/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : CrosshairBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import PropertyManager from "../../../../Manager/PropertyManager";

import PlaneBuilder from "../../Object/Plane/PlaneBuilder";

export default class CrosshairBuilder {
    /**
     * Build Crosshair.
     *
     * @param options
     * @returns {Promise<*>}
     */
    static build(options = {}) {
        // Prepare variables
        let chOptions = PropertyManager.mergeObject(options,  {
            id: 'CrosshairId',
            name: 'Crosshair',
            status: 0,
        });
        
        return PlaneBuilder.build(chOptions);
    }
}