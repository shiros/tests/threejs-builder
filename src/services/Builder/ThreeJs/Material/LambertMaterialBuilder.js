/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : LambertMaterialBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {MeshLambertMaterial} from 'three';
import AbstractMaterialBuilder from "./AbstractMaterialBuilder";

export default class LambertMaterialBuilder extends AbstractMaterialBuilder {
    // ------------------------
    // Load
    
    /**
     * Load Material.
     *
     * @param options
     * @returns {Promise<MeshLambertMaterial>}
     */
    static async loadMaterial(options = {}) {
        return new MeshLambertMaterial({
            side: this.getSide(options),
            transparent: this.getTransparency(options),
            needsUpdate: true
        });
    }
}