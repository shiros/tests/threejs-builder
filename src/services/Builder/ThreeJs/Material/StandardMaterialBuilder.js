/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : StandardMaterialBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {MeshStandardMaterial} from 'three';
import AbstractMaterialBuilder from "./AbstractMaterialBuilder";

export default class StandardMaterialBuilder extends AbstractMaterialBuilder {
    // ------------------------
    // Load
    
    /**
     * Load Material.
     *
     * @param options
     * @returns {Promise<*>}
     */
    // static async loadMaterial(options = {}) {
    static loadMaterial(options = {}) {
        return new Promise((resolve) => {
            resolve(new MeshStandardMaterial({
                roughness: this.getRoughness(options),
                metalness: this.getMetalness(options),
    
                opacity: this.getOpacity(options),
                transparent: this.getTransparency(options),
    
                wireframe: this.getWireframe(options),
    
                side: this.getSide(options),
                needsUpdate: true
            }));
        });
    }
}