/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : MaterialBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import ObjectManager from "../../../Manager/ObjectManager";

import BasicMaterialBuilder from "./BasicMaterialBuilder";
import StandardMaterialBuilder from "./StandardMaterialBuilder";
import LambertMaterialBuilder from "./LambertMaterialBuilder";
import LineBasicMaterialBuilder from "./LineBasicMaterialBuilder";

export default class MaterialBuilder {
    /**
     * Build Material.
     *
     * @param options
     * @returns {Promise<Material>}
     */
    static build(options = {}) {
        let type = ObjectManager.hasProperty(options, 'type') ? options.type : 'standard';

        switch (type) {
            case 'basic':
                return BasicMaterialBuilder.build(options);
    
            default:
            case 'standard':
                return StandardMaterialBuilder.build(options);

            case 'lambert':
                return LambertMaterialBuilder.build(options);

            case 'lineBasic':
                return LineBasicMaterialBuilder.build(options);
        }
    }
}