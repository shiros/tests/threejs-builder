/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : AbstractMaterialBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */

import {
    // Material
    TextureLoader,
    FrontSide,
    BackSide,
    DoubleSide,
    
    // Utils
    Color,
    Vector2
} from 'three';

import FileManager from "../../../Manager/FileManager";
import ObjectManager from "../../../Manager/ObjectManager";
import PropertyManager from "../../../Manager/PropertyManager";
import UrlModule from "../../../Module/Url/UrlModule";
import Logger from "../../../Log/Logger";
import TypeManager from "../../../Manager/TypeManager";

export default class AbstractMaterialBuilder {
    
    /**
     * Abstract MAterial Builder Constructor.
     */
    constructor() {
        if (this.constructor === AbstractMaterialBuilder) {
            throw new TypeError('Abstract class "AbstractObjectBuilder" cannot be instantiated directly');
        }
    }
    
    // ------------------------
    // Build
    
    /**
     * Build Material.
     *
     * @param options
     * @returns {Promise<*>}
     */
    static async build(options = {}) {
        let material = await this.loadMaterial(options);
        
        this.prepareTexture(material, options);
        this.prepareColor(material, options);
        this.prepareAlpha(material, options);
        this.prepareEmissive(material, options);
        this.prepareBump(material, options);
        this.prepareNormal(material, options);
        
        return material;
    }
    
    // ------------------------
    // Prepare
    
    /**
     * Prepare Texture of Material.
     *
     * @param material
     * @param options
     * @returns void
     */
    static prepareTexture(material, options = {}) {
        if (!ObjectManager.hasProperty(material, 'map', true)) return;
        
        let texture = this.getTexture(options);
        if (TypeManager.isNull(texture)) return;
        
        this.loadTexture(texture)
            .then((map) => {
                material.map = map;
                material.map.needsUpdate = true;
                material.needsUpdate = true;
            })
            .catch(Logger.error)
        ;
    }
    
    /**
     * Prepare Color of Material.
     *
     * @param material
     * @param options
     * @returns void
     */
    static prepareColor(material, options = {}) {
        if (!ObjectManager.hasProperty(material, 'color', true)) return;
        
        let color = this.getColor(options);
        if (TypeManager.isNull(color)) return;
        
        this.loadColor(color)
            .then(color => material.color = color)
            .catch(Logger.error)
        ;
    }
    
    /**
     * Prepare Alpha of Material.
     *
     * @param material
     * @param options
     * @returns void
     */
    static prepareAlpha(material, options = {}) {
        if (!ObjectManager.hasProperty(material, 'alphaMap', true)) return;
        if (!ObjectManager.hasProperty(options, 'alpha')) return;
        
        let alphaMap = this.getAlphaMap(options);
        if (TypeManager.isNull(alphaMap)) return;
        
        this.loadAlphaMap(alphaMap)
            .then(alphaMap => {
                material.alphaMap = alphaMap;
                material.alphaMap.needsUpdate = true;
                material.needsUpdate = true;
            })
            .catch(Logger.error)
        ;
    }
    
    /**
     * Prepare Bump of Material.
     *
     * @param material
     * @param options
     * @returns void
     */
    static prepareBump(material, options = {}) {
        if (!ObjectManager.hasProperty(material, 'bumpMap', true)) return;
        if (!ObjectManager.hasProperty(options, 'bump')) return;
        
        let bumpMap = this.getBumpMap(options);
        if (TypeManager.isNull(bumpMap)) return;
        
        this.loadBumpMap(bumpMap)
            .then(bumpMap => {
                material.bumpMap = bumpMap;
                material.bumpMap.needsUpdate = true;
                material.needsUpdate = true;
                
                // Scale
                if (ObjectManager.hasProperty(material, 'bumpScale', true)) {
                    material.bumpScale = ObjectManager.hasProperty(options.bump, 'scale')
                        ? Number(options.bumpScale) : 1;
                }
            })
            .catch(Logger.error)
        ;
    }
    
    /**
     * Prepare Emissive of Material.
     *
     * @param material
     * @param options
     * @returns void
     */
    static prepareEmissive(material, options = {}) {
        if (!ObjectManager.hasProperty(material, 'emissiveMap', true)) return;
        if (!ObjectManager.hasProperty(options, 'emissive')) return;
        
        let emissiveMap = this.getEmissiveMap(options);
        if (TypeManager.isNull(emissiveMap)) return;
        
        this.loadEmissiveMap(emissiveMap)
            .then(emissiveMap => {
                material.emissiveMap = emissiveMap;
                material.emissiveMap.needsUpdate = true;
                material.needsUpdate = true;
            })
            .catch(Logger.error)
        ;
        
        if (ObjectManager.hasProperty(material, 'emissive', true)) {
            let emissiveColor = this.getColor(options.emissive);
            
            if (TypeManager.isHexColor(emissiveColor)) material.emissive = new Color(options.emissive.color);
        }
        
        if (ObjectManager.hasProperty(material, 'emissiveIntensity', true)) {
            material.emissiveIntensity = ObjectManager.hasProperty(options.emissive, 'intensity')
                ? Number(options.emissive.intensity) : 0.5;
        }
    }
    
    /**
     * Prepare Normal of Material.
     *
     * @param material
     * @param options
     * @returns void
     */
    static prepareNormal(material, options = {}) {
        if (!ObjectManager.hasProperty(material, 'normalMap', true)) return;
        if (!ObjectManager.hasProperty(options, 'normal')) return;
        
        let normalMap = this.getNormalMap(options);
        if (TypeManager.isNull(normalMap)) return;
        
        this.loadNormalMap(normalMap)
            .then(normalMap => {
                material.normalMap = normalMap;
                material.normalMap.needsUpdate = true;
                material.needsUpdate = true;
                
                // Scale
                if (ObjectManager.hasProperty(material, 'normalScale', true)) {
                    let normalScale = ObjectManager.hasProperty(options.normal, 'scale') ? options.normal.scale : {};
                    material.normalScale = new Vector2(
                        ObjectManager.hasProperty(normalScale, 'x') ? Number(normalScale.x) : 1,
                        ObjectManager.hasProperty(normalScale, 'y') ? Number(normalScale.y) : 1
                    );
                }
            })
            .catch(Logger.error)
        ;
    }
    
    // ------------------------
    // Load
    
    /**
     * Load Material.
     *
     * @param options
     * @returns {Promise<*>}
     */
    // eslint-disable-next-line no-unused-vars
    static async loadMaterial(options = {}) {
        throw new Error("You must be override the 'loadMaterial' method before use it.");
    }
    
    /**
     * Load Texture.
     *
     * @param map
     * @returns {Promise<*>}
     */
    static loadTexture(map) {
        return new Promise((resolve, reject) => {
            if (TypeManager.isNull(map)) {
                reject("Map can't be null. Can't load the texture.");
            }
            
            FileManager.existsAsync(UrlModule.resourcePath(map))
                .then((path) => {
                    (new TextureLoader()).load(
                        path,
                        (map) => {
                            resolve(map);
                        },
                        null,
                        (e) => {
                            reject(e);
                        }
                    );
                })
                .catch((path) => {
                    reject(`Error to map : ${path}.`);
                })
            ;
        });
    }
    
    /**
     * Load Color.
     *
     * @param color
     * @returns {Promise<Color>}
     */
    static async loadColor(color) {
        if (!TypeManager.isHexColor(color)) {
            throw new Error("Color need to be an hexadecimal code. Can't load the color.");
        }
        
        return new Color(color);
    }
    
    /**
     * Load Alpha Map.
     *
     * @param alphaMap
     * @returns {Promise<*>}
     */
    static loadAlphaMap(alphaMap) {
        return new Promise((resolve, reject) => {
            if (TypeManager.isNull(alphaMap)) {
                reject("Alpha Map can't be null. Can't load the alpha map.");
            }
            
            FileManager.existsAsync(UrlModule.resourcePath(alphaMap))
                .then((path) => {
                    (new TextureLoader()).load(
                        path,
                        (map) => {
                            resolve(map);
                        },
                        null,
                        (e) => {
                            reject(e);
                        }
                    );
                })
                .catch((path) => {
                    reject(`Error to load alpha map : ${path}.`);
                })
            ;
        });
    }
    
    /**
     * Load Bump Map.
     *
     * @param bumpMap
     * @returns {Promise<*>}
     */
    static loadBumpMap(bumpMap) {
        return new Promise((resolve, reject) => {
            if (TypeManager.isNull(bumpMap)) {
                reject("Bump Map can't be null. Can't load the bump map.");
            }
    
            FileManager.existsAsync(UrlModule.resourcePath(bumpMap))
                .then((path) => {
                    (new TextureLoader()).load(
                        path,
                        (map) => {
                            resolve(map);
                        },
                        null,
                        (e) => {
                            reject(e);
                        }
                    );
                })
                .catch((path) => {
                    reject(`Error to load bump map : ${path}.`);
                })
            ;
        });
    }
    
    /**
     * Load Emisive Map.
     *
     * @param emissiveMap
     * @returns {Promise<*>}
     */
    static loadEmissiveMap(emissiveMap) {
        return new Promise((resolve, reject) => {
            if (TypeManager.isNull(emissiveMap)) {
                reject("Emissive Map can't be null. Can't load the emissive map.");
            }
            
            FileManager.existsAsync(UrlModule.resourcePath(emissiveMap))
                .then((path) => {
                    (new TextureLoader()).load(
                        path,
                        (map) => {
                            resolve(map);
                        },
                        null,
                        (e) => {
                            reject(e);
                        }
                    );
                })
                .catch((path) => {
                    reject(`Error to load emissive map : ${path}.`);
                })
            ;
        })
    }
    
    /**
     * Load Normal Map.
     *
     * @param normalMap
     * @returns {Promise<*>}
     */
    static async loadNormalMap(normalMap) {
        return new Promise((resolve, reject) => {
            if (TypeManager.isNull(normalMap)) {
                reject("Normal Map can't be null. Can't load the normal map.");
            }
    
            FileManager.existsAsync(UrlModule.resourcePath(normalMap))
                .then((path) => {
                    (new TextureLoader()).load(
                        path,
                        (map) => {
                            resolve(map);
                        },
                        null,
                        (e) => {
                            reject(e);
                        }
                    );
                })
                .catch((path) => {
                    reject(`Error to load normal map : ${path}.`);
                })
            ;
        })
    }
    
    // ------------------------
    // Getters
    
    /**
     * Get Texture.
     *
     * @param options
     * @param defaultValue
     * @return string
     */
    static getTexture(options = {}, defaultValue = null) {
        let texture = ObjectManager.hasProperty(options, 'map') ? options.map : defaultValue;
        if (ObjectManager.hasProperty(texture, 'value')) texture = texture.value;
        
        return texture;
    }
    
    /**
     * Get Color.
     *
     * @param options
     * @param defaultValue
     * @return number
     */
    static getColor(options = {}, defaultValue = null) {
        let color = ObjectManager.hasProperty(options, 'color') ? options.color : defaultValue;
        if (ObjectManager.hasProperty(color, 'value')) color = color.value;
        
        return color;
    }
    
    /**
     * Get Metalness.
     *
     * @param options
     * @param defaultValue
     * @return number
     */
    static getMetalness(options = {}, defaultValue = 0) {
        let metalness = ObjectManager.hasProperty(options, 'metalness') ? options.metalness : defaultValue;
        if (ObjectManager.hasProperty(metalness, 'value')) metalness = metalness.value;
        
        return metalness;
    }
    
    /**
     * Get Roughness.
     *
     * @param options
     * @param defaultValue
     * @return number
     */
    static getRoughness(options = {}, defaultValue = 1) {
        let roughness = ObjectManager.hasProperty(options, 'roughness') ? options.roughness : defaultValue;
        if (ObjectManager.hasProperty(roughness, 'value')) roughness = roughness.value;
        
        return roughness;
    }
    
    /**
     * Get Wireframe.
     *
     * @param options
     * @param defaultValue
     * @return boolean
     */
    static getWireframe(options = {}, defaultValue = false) {
        let wireframe = ObjectManager.hasProperty(options, 'wireframe') ? options.wireframe : defaultValue;
        if (ObjectManager.hasProperty(wireframe, 'value')) wireframe = wireframe.value;
        
        return PropertyManager.getBoolean(wireframe);
    }
    
    /**
     * Get Opacity.
     *
     * @param options
     * @param defaultValue
     * @returns {number}
     */
    static getOpacity(options = {}, defaultValue = 1) {
        let opacity = ObjectManager.hasProperty(options, 'opacity') ? Number(options.opacity) : defaultValue;
        if (ObjectManager.hasProperty(opacity, 'value')) opacity = Number(opacity.value);
        
        return opacity;
    }
    
    /**
     * Get Transparency.
     *
     * @param options
     * @param defaultValue
     * @return boolean
     */
    static getTransparency(options = {}, defaultValue = false) {
        let transparency = ObjectManager.hasProperty(options, 'transparency') ? options.transparency : defaultValue;
        if (ObjectManager.hasProperty(transparency, 'value')) transparency = transparency.value;
        
        return PropertyManager.getBoolean(transparency);
    }
    
    /**
     * Get Side of material
     *
     * @param options
     * @return Side
     */
    static getSide(options = {}) {
        let sideCondition = ObjectManager.hasProperty(options, 'side');
        let side = sideCondition ? options.side : null;
        
        let threeSide = null;
        switch (side) {
            default:
            case 'front':
                threeSide = FrontSide;
                break;
            
            case 'back':
                threeSide = BackSide;
                break;
            
            case 'double':
                threeSide = DoubleSide;
                break;
        }
        
        return threeSide;
    }
    
    /**
     * Get Alpha Map.
     *
     * @param options
     * @param defaultValue
     * @return string
     */
    static getAlphaMap(options = {}, defaultValue = null) {
        let alpha = ObjectManager.hasProperty(options, 'alpha') ? options.alpha : {};
        
        let alphaMap = ObjectManager.hasProperty(alpha, 'map') ? alpha.map : defaultValue;
        if (ObjectManager.hasProperty(alphaMap, 'value')) alphaMap = alphaMap.value;
        
        return alphaMap;
    }
    
    /**
     * Get Bump Map.
     *
     * @param options
     * @param defaultValue
     * @return string
     */
    static getBumpMap(options = {}, defaultValue = null) {
        let bump = ObjectManager.hasProperty(options, 'bump') ? options.bump : {};
        
        let bumpMap = ObjectManager.hasProperty(bump, 'map') ? bump.map : defaultValue;
        if (ObjectManager.hasProperty(bumpMap, 'value')) bumpMap = bumpMap.value;
        
        return bumpMap;
    }
    
    /**
     * Get Emissive Map.
     *
     * @param options
     * @param defaultValue
     * @return string
     */
    static getEmissiveMap(options = {}, defaultValue = null) {
        let emissive = ObjectManager.hasProperty(options, 'emissive') ? options.emissive : {};
        
        let emissiveMap = ObjectManager.hasProperty(emissive, 'map') ? emissive.map : defaultValue;
        if (ObjectManager.hasProperty(emissiveMap, 'value')) emissiveMap = emissiveMap.value;
        
        return emissiveMap;
    }
    
    /**
     * Get Normal Map.
     *
     * @param options
     * @param defaultValue
     * @return string
     */
    static getNormalMap(options = {}, defaultValue = null) {
        let normal = ObjectManager.hasProperty(options, 'normal') ? options.normal : {};
        
        let normalMap = ObjectManager.hasProperty(normal, 'map') ? normal.map : defaultValue;
        if (ObjectManager.hasProperty(normalMap, 'value')) normalMap = normalMap.value;
        
        return normalMap;
    }
}