/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : LineBasicMaterialBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {LineBasicMaterial} from 'three';
import AbstractMaterialBuilder from "./AbstractMaterialBuilder";

import ObjectManager from "../../../Manager/ObjectManager";

export default class LineBasicMaterialBuilder extends AbstractMaterialBuilder {
    // ------------------------
    // Load
    
    /**
     * Load Material.
     *
     * @param options
     * @returns {Promise<LineBasicMaterial>}
     */
    // eslint-disable-next-line no-unused-vars
    static async loadMaterial(options = {}) {
        return new LineBasicMaterial({
            linewidth: this.getLineWidth(options)
        });
    }
    
    // ------------------------
    // Getters
    
    /**
     * Get Line Width.
     *
     * @param options
     * @returns {number}
     */
    static getLineWidth(options = {}) {
        let lineWidth = ObjectManager.hasProperty(options, 'lineWidth') ? options.lineWidth : 1;
        if (ObjectManager.hasProperty(lineWidth, 'value')) lineWidth = lineWidth.value;
    
        return Number(lineWidth);
    }
}