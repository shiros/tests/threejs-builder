/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : BasicMaterialBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {MeshBasicMaterial} from 'three';
import AbstractMaterialBuilder from "./AbstractMaterialBuilder";

export default class BasicMaterialBuilder extends AbstractMaterialBuilder {
    // ------------------------
    // Load
    
    /**
     * Load Material.
     *
     * @param options
     * @returns {Promise<MeshBasicMaterial>}
     */
    static async loadMaterial(options = {}) {
        return new MeshBasicMaterial({
            side: this.getSide(options),
            opacity: this.getOpacity(options),
            transparent: this.getTransparency(options),
            needsUpdate: true,
        });
    }
}