/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : SceneBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {Scene, Color} from 'three';

import ObjectManager from "../../../Manager/ObjectManager";
import PropertyManager from "../../../Manager/PropertyManager";
import TypeManager from "../../../Manager/TypeManager";

export default class SceneBuilder {
    /**
     * Build Scene.
     *
     * @param options
     * @returns {Scene}
     */
    static build(options = {}) {
        // Prepare variables
        let transparency = ObjectManager.hasProperty(options, 'transparency')
            ? PropertyManager.getBoolean(options.transparency) : false;
        let color = ObjectManager.hasProperty(options, 'color') && TypeManager.isHexColor(options.color)
            ? options.color : '#212121';
        
        // Create Scene
        let scene = new Scene();
        
        // Settings post build
        scene.background = (transparency) ? null : new Color(color);
        
        return scene;
    }
}