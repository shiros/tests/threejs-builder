/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : RaycasterBuilder.js
 * @Created_at : 15/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */
import {Raycaster} from 'three';

export default class RaycasterBuilder {
    // ------------------------
    // Build
    
    /**
     * @return {Raycaster}
     */
    static build() {
        return new Raycaster();
    }
}