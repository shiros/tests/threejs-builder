/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : DirectionalLightBuilder.js
 * @Created_at : 14/10/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {DirectionalLight, Color} from "three";

import ObjectManager from "../../../Manager/ObjectManager";
import TypeManager from "../../../Manager/TypeManager";

export default class DirectionalLightBuilder {
    /**
     * Build Directional Light.
     *
     * @param options
     * @returns {Promise<DirectionalLight>}
     */
    static async build(options = {}) {
        // Prepare variables
        let color = ObjectManager.hasProperty(options, 'color') && TypeManager.isHexColor(options.color)
            ? options.color : '#FFFFFF';
        let intensity = ObjectManager.hasProperty(options, 'intensity') ? Number(options.intensity) : 1;
        
        let position = ObjectManager.hasProperty(options, 'position') ? options.position : {};
        let positionX = ObjectManager.hasProperty(position, 'x') ? Number(position.x) : 0;
        let positionY = ObjectManager.hasProperty(position, 'y') ? Number(position.y) : 0;
        let positionZ = ObjectManager.hasProperty(position, 'z') ? Number(position.z) : 0;
    
        // Create Directional Light
        let light = new DirectionalLight();
        
        // Settings post build
        light.color = new Color(color);
        light.intensity = intensity;
        light.position.set(positionX, positionY, positionZ);
        
        return light;
    }
}