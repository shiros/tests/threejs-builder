/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : SpotLightBuilder.js
 * @Created_at : 20/02/2019
 * @Update_at : 15/10/2019
 * --------------------------------------------------------------------------
 */

import {SpotLight, Color} from "three";

import ObjectManager from "../../../Manager/ObjectManager";
import TypeManager from "../../../Manager/TypeManager";

export default class SpotLightBuilder {
    /**
     * Build Spot Light.
     *
     * @param options
     * @returns {Promise<SpotLight>}
     */
    static async build(options = {}) {
        // Prepare variables
        let color = ObjectManager.hasProperty(options, 'color') && TypeManager.isHexColor(options.color)
            ? options.color : '#FFFFFF';
        let angle = ObjectManager.hasProperty(options, 'angle') ? Number(options.angle) : 15;
        let intensity = ObjectManager.hasProperty(options, 'intensity') ? Number(options.intensity) : 200;
        let decay = ObjectManager.hasProperty(options, 'decay') ? Number(options.decay) : 1;
        let penumbra = ObjectManager.hasProperty(options, 'penumbra') ? Number(options.penumbra) : 1;
        let distance = ObjectManager.hasProperty(options, 'distance') ? Number(options.distance) : 100;
        
        let position = ObjectManager.hasProperty(options, 'position') ? options.position : {};
        let positionX = ObjectManager.hasProperty(position, 'x') ? Number(position.x) : 0;
        let positionY = ObjectManager.hasProperty(position, 'y') ? Number(position.y) : 0;
        let positionZ = ObjectManager.hasProperty(position, 'z') ? Number(position.z) : 0;
        
        // Create Spot Light
        let light = new SpotLight();
        
        // Settings post build
        light.color = new Color(color);
        light.angle = Math.PI / 180 * angle;
        light.intensity = intensity;
        light.decay = decay;
        light.penumbra = penumbra;
        light.distance = distance;
        light.position.set(positionX, positionY, positionZ);
        
        return light;
    }
}