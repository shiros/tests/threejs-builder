/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : Animation.js
 * @Created_at : 17/10/2019
 * @Update_at : 17/10/2019
 * --------------------------------------------------------------------------
 */
import FluxAnimation from "./Type/FluxAnimation";
import RotateAnimation from "./Type/RotateAnimation";
import WaveAnimation from "./Type/WaveAnimation";
import TestAnimation from "./Type/TestAnimation";

export default class Animation {
    // ------------------------
    // Animate

    /**
     * Animate an Three Js Object
     *
     * @param name
     * @param object
     */
    static animate(name, object) {
        switch (name) {
            case 'flux':
                FluxAnimation.animate(object);
                break;

            case 'rotate':
                RotateAnimation.animate(object);
                break;

            case 'wave':
                WaveAnimation.animate(object);
                break;

            case 'test':
                TestAnimation.animate(object);
                break;

            default:
                break;
        }
    }
}