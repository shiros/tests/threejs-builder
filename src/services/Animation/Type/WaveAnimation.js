/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : WaveAnimation.js
 * @Created_at : 17/10/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */
import ObjectManager from "../../Manager/ObjectManager";

export default class WaveAnimation {

    // ------------------------
    // Animate

    /**
     * Animate an Three Js Object
     *
     * @param object
     */
    static animate(object) {
        if (!ObjectManager.hasProperty(object, 'id')
            
            || !ObjectManager.hasProperty(object, 'default')
            || !ObjectManager.hasProperty(object.default, 'position')
            || !ObjectManager.hasProperty(object.default.position, 'y')

            || !ObjectManager.hasProperty(object, 'default')
            || !ObjectManager.hasProperty(object.default, 'rotation')
            || !ObjectManager.hasProperty(object.default.rotation, 'z')
        ) return;

        let time = Date.now();
        let factorTime = 0.001;

        // Position
        object.position.y = ( 5 * Math.cos( (time * factorTime) + object.id ) ) + parseInt(object.default.position.y, 10);

        // Rotation
        object.rotation.z = ( 0.1 * Math.sin( (time * factorTime) + object.id ) ) + parseInt(object.default.rotation.z, 10);
    }
}