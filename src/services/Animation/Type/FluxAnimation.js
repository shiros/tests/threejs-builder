/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : FluxAnimation.js
 * @Created_at : 17/10/2019
 * @Update_at : 17/10/2019
 * --------------------------------------------------------------------------
 */
import ObjectManager from "../../Manager/ObjectManager";

export default class FluxAnimation {

    // ------------------------
    // Animate

    /**
     * Animate an Three Js Object
     *
     * @param object
     */
    static animate(object) {
        if (!ObjectManager.hasProperty(object, 'id')) return;

        let time = Date.now();
        let factorTime = 0.00002;

        // Position
        object.position.z = 5000 * Math.sin( ((time * factorTime) + object.id) * 1.1);
    }
}