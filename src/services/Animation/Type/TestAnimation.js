/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : TestAnimation.js
 * @Created_at : 17/10/2019
 * @Update_at : 18/10/2019
 * --------------------------------------------------------------------------
 */
import ObjectManager from "../../Manager/ObjectManager";

export default class TestAnimation {

    // ------------------------
    // Animate

    /**
     * Animate an Three Js Object
     *
     * @param object
     */
    static animate(object) {
        if (!ObjectManager.hasProperty(object, 'id')) return;
    }
}